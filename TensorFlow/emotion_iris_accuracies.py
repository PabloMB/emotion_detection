##Configure imports and eager execution
from __future__ import absolute_import, division, print_function

import os
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow.contrib.eager as tfe

import datetime


##parameters
initial_it = 1 #load the model at the iteration saved that is desired
jump = 25 #number of saved files to skip + 1
#results
folder = "emotion_iris_results/"
subfolder = "run6/"
model_to_load = "model_7000_89-80"
#if not os.path.exists(folder):
#    os.makedirs(folder)
#if not os.path.exists(folder+subfolder):
#    os.makedirs(folder+subfolder)
##model_to_load = "model_"+str(initial_it)
##model_h5_to_load = folder+subfolder + model_to_load+".h5"
##model_h5_to_save = folder+subfolder + "model_"   #iteration and extension are added later
##model_json_to_load = folder+subfolder + model_to_load+".json"
##model_json_to_save = folder+subfolder + "model_" #iteration and extension are added later

#databases
#path for IIT PC
filepath = "C:/Users/pmenendezblanco/VisualStudio_projects/superviseddescent_project/superviseddescent_project/results/"+subfolder+"databaseLandmarks3D_full/"
train_filename = filepath + "results_8emotions_normalized_balanced_0to1_train.csv"
test_filename = filepath + "results_8emotions_normalized_balanced_0to1_test.csv"
###path for my PC
##filepath = "D:/Users/Pablo/Documentos/Emotion/superviseddescent_models/"
##train_filename = filepath+subfolder + "results_8emotions_normalized_balanced_train.csv"
##test_filename = filepath+subfolder + "results_8emotions_normalized_balanced_test.csv"

###model parameters
inputs = 68*2
outputs = 8
##N = inputs*2
##A = "relu"
##init = "random_uniform"
##bias = "zeros"
##D = 0.25
###data parameters
batch_size = 100
batch_percentage = 0.1
###training parameters
##learning_rate = 0.01
##num_epochs = 2200



tf.enable_eager_execution()

print("TensorFlow version: {}".format(tf.VERSION))
print("Eager execution: {}".format(tf.executing_eagerly()))

def print_current_time():
  print(datetime.datetime.now())

print_current_time()






#Count number of samples for each output
class_samples = [0] * outputs
total_samples = 0
##for i in range(len(class_samples)):
##  class_samples[i] = 0
def count_samples(dataset0): #introduce dataset without batches
  global class_samples
  global total_samples
  class_samples = [0] * outputs
  total_samples = 0
  for features, labels in dataset0:
    #for lab in labels: #this line would work if the dataset had batches
      i = labels.numpy().astype(int)
      class_samples[i] = class_samples[i] + 1
  for i in range(len(class_samples)):
    print("class {} -> {} samples".format(i,class_samples[i]))
    total_samples = total_samples + class_samples[i]
  print("total: {} samples".format(total_samples))




  


##Import and parse the datasets
def parse_csv(line):
  example_defaults = [[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],
                      [0]]  # sets field types
  parsed_line = tf.decode_csv(line, example_defaults)
  # First fields are features, combine into single tensor
  features = tf.reshape(parsed_line[:inputs], shape=(inputs,))
  # Last field is the label
  label = tf.reshape(parsed_line[-1], shape=())
  return features, label

#Load trainset
train_dataset0 = tf.data.TextLineDataset(train_filename)
#train_dataset = train_dataset.skip(1)                      # skip the first header row
train_dataset0 = train_dataset0.map(parse_csv)              # parse each row
train_dataset0 = train_dataset0.shuffle(buffer_size=1000) # randomize
count_samples(train_dataset0) #count total samples
print("total: {} samples".format(total_samples))
batch_size = int(total_samples * batch_percentage) #make batches
train_dataset_batched = train_dataset0.batch(batch_size)
print("train batch_size:", batch_size)
#train_dataset_batched = train_dataset0.apply(tf.contrib.data.batch_and_drop_remainder(batch_size)) #does not work

# View a single example entry from a batch
features, label = iter(train_dataset_batched).next()
print("example features:", features[0])
print("example label:", label[0])


#Load testset
test_dataset0 = tf.data.TextLineDataset(test_filename)
#test_dataset = test_dataset.skip(1)                      # skip the first header row
test_dataset0 = test_dataset0.map(parse_csv)                # parse each row
test_dataset0 = test_dataset0.shuffle(buffer_size=1000)   # randomize
count_samples(test_dataset0) #count total samples
batch_size = int(total_samples * batch_percentage) #make batches
test_dataset_batched = test_dataset0.batch(batch_size)
print("test batch_size:", batch_size)
#test_dataset_batched = test_dataset0.apply(tf.contrib.data.batch_and_drop_remainder(batch_size)) #does not work

# View a single example entry from a batch
features, label = iter(test_dataset_batched).next()
print("example features:", features[0])
print("example label:", label[0])






  



###Select the type of model
##model = tf.keras.Sequential([
##  tf.keras.layers.Dense(N, activation=A, input_shape=(inputs,)),  # input shape required (# of features)
##  tf.keras.layers.Dense(N, activation=A),
##  tf.keras.layers.Dense(N, activation=A),
##  tf.keras.layers.Dense(N, activation=A),
##  tf.keras.layers.Dense(outputs)
##])


##load model if saved before
def load_model(modelname):
  model_h5_to_load = folder+subfolder + modelname+".h5"
  model_json_to_load = folder+subfolder + modelname+".json"
  if os.path.exists(model_json_to_load) and os.path.exists(model_h5_to_load):
    json_file = open(model_json_to_load, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = tf.keras.models.model_from_json(loaded_model_json)
    model.load_weights(model_h5_to_load)
    print("\nModel saved loaded\n")
  else:
    model = None
    print("\nModel not loaded\n")
  return model

##function to save the model
def save_model(model, it):
  model_json = model.to_json()
  f = open(model_json_to_save+str(initial_it+it)+".json", "w")
  f.write(model_json)
  f.close()
  model.save_weights(model_h5_to_save+str(initial_it+it)+".h5")

  f = open(folder+subfolder + 'params.txt','w')
  s = str(batch_size) + "\n" + str(learning_rate) + "\n" + str(num_epochs)
  f.write(s)
  f.close()
  
  print("\nMODEL SAVED\n")


#Train the model
def loss(model, x, y):
  y_ = model(x)
  return tf.losses.sparse_softmax_cross_entropy(labels=y, logits=y_)

def grad(model, inputs, targets):
  with tf.GradientTape() as tape:
    loss_value = loss(model, inputs, targets)
  return tape.gradient(loss_value, model.variables)






##no need yet for this variables to be global
##class_hits = [0] * outputs      #true positives
##class_total = [0] * outputs     #true positives + false positives, to calculate accuracy
##class_accuracy = [0] * outputs  #accuracy
##class_total2 = [0] * outputs    #true positives + false negatives, to calculate recall
##class_recall = [0] * outputs    #recall
##hits = 0
##total = 0
##accuracy = 0
##total2 = 0
##recall = 0
def obtain_accuracies(model,dataset):
  class_hits = [0] * outputs      #true positives
  class_total = [0] * outputs     #true positives + false positives, to calculate accuracy
  class_accuracy = [0] * outputs  #accuracy
  class_total2 = [0] * outputs    #true positives + false negatives, to calculate recall
  class_recall = [0] * outputs    #recall
  hits = 0
  total = 0
  accuracy = 0
  total2 = 0
  recalll = 0
  for x, y in dataset:
    y_ = tf.argmax(model(x), axis=1, output_type=tf.int32)
    length = tf.size(y).numpy().astype(int)
    for i in range(length):
      a = y_[i].numpy().astype(int)
      b = y[i].numpy().astype(int)
      if a==b:
        class_hits[b] = class_hits[b] + 1   #count every time that it is right (true positives)
      class_total[b] = class_total[b] + 1   #count every time that it is emotion b
      class_total2[a] = class_total2[a] + 1 #count every time that it says that it is emotion a 
  for i in range(outputs):
    class_accuracy[i] = float(class_hits[i]) / class_total[i]
    if class_total2[i]==0:
      class_recall[i] = float(0)
    else:
      class_recall[i] = float(class_hits[i]) / class_total2[i]
    hits = hits + class_hits[i]
    total = total + class_total[i]
    total2 = total2 + class_total2[i]
    print("-> emotion {}: accuracy: {:4d} / {:4d} = {:.2f}     |     recall: {:4d} / {:4d} = {:.2f}".format(
      i,class_hits[i],class_total[i],class_accuracy[i],class_hits[i],class_total2[i],class_recall[i]))
  accuracy = float(hits)/total
  recall = float(hits)/total2
  print("total accuracy: {:4d} / {:4d} = {:.2f}     |     total recall: {:4d} / {:4d} = {:.2f}".format(
    hits,total,accuracy,hits,total2,recall))

##print("\nTrain accuracies:")
##obtain_accuracies(model,train_dataset_batched)
##print("\nTest accuracies:")
##obtain_accuracies(model,test_dataset_batched)







#Get accuracies of models

def print_accuracies(modelname):
  model = load_model(modelname)
  if model!=None:
    train_loss_avg = tfe.metrics.Mean()
    epoch_accuracy = tfe.metrics.Accuracy()
    
    # Training loop - using batches
    for x, y in train_dataset_batched:
      # Track progress
      train_loss_avg(loss(model, x, y))  # add current batch loss
      # compare predicted label to actual label
      epoch_accuracy(tf.argmax(model(x), axis=1, output_type=tf.int32), y)

    print_current_time()
    print("Train loss: {:.3f}, Train accuracy: {:.3%}"
          .format(train_loss_avg.result(),epoch_accuracy.result()))

    #Obtaining test accuracy
    test_loss_avg = tfe.metrics.Mean()
    test_accuracy = tfe.metrics.Accuracy()
    for (x, y) in test_dataset_batched:
      prediction = tf.argmax(model(x), axis=1, output_type=tf.int32)
      test_loss_avg(loss(model, x, y))
      test_accuracy(prediction, y)
    print_current_time()
    print("Test loss: {:.3f},  Test accuracy: {:.3%}"
          .format(test_loss_avg.result(),test_accuracy.result()))

    print("\nTrain accuracies:")
    obtain_accuracies(model,train_dataset_batched)
    print("\nTest accuracies:")
    obtain_accuracies(model,test_dataset_batched)
  else:
    print("model {} not found".format(model_to_load))


print_accuracies(model_to_load)
