# View more python learning tutorial on my Youtube and Youku channel!!!

# Youtube video tutorial: https://www.youtube.com/channel/UCdyjiB5H8Pu7aDTNVXTTpcg
# Youku video tutorial: http://i.youku.com/pythontutorial

"""
Please note, this code is only for python 3+. If you are using python 2+, please modify the code accordingly.
"""
from __future__ import print_function
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

filepath = "C:/Users/pmenendezblanco/VisualStudio_projects/superviseddescent_project/superviseddescent_project/results/run5/databaseLandmarks_full/"
filename = filepath + "/results_normalized_with_ones.csv"
ds = tf.data.TextLineDataset(filename)#.skip(1)

dataset = tf.contrib.learn.datasets.base.load_csv_without_header(
      filename=filename,
      target_dtype=np.int,
      features_dtype=np.float32)


def add_layer(inputs, in_size, out_size, activation_function=None):
    # add one more layer and return the output of this layer
    Weights = tf.Variable(tf.random_normal([in_size, out_size]))
    biases = tf.Variable(tf.zeros([1, out_size]) + 0.1)
    Wx_plus_b = tf.matmul(inputs, Weights) + biases
    if activation_function is None:
        outputs = Wx_plus_b
    else:
        outputs = activation_function(Wx_plus_b)
    return outputs

# Make up some real data
#x_data = np.linspace(-1,1,300)[:, np.newaxis]
#noise = np.random.normal(0, 0.05, x_data.shape)
#y_data = np.square(x_data) - 0.5 + noise
x_data = dataset.data[0:1][0:136]
y_data = dataset.data[0:1][139:149]
print(tf.size(y_data))
x_data = tf.convert_to_tensor(x_data)
y_data = tf.convert_to_tensor(y_data)
print(tf.size(y_data))
x_data = tf.transpose(x_data)
y_data = tf.transpose(y_data)
print(tf.size(y_data))
print(y_data)

# define placeholder for inputs to network
xs = tf.placeholder(tf.float32, [None, 136])
ys = tf.placeholder(tf.float32, [None, 10])
# add hidden layer
l1 = add_layer(xs, 136, 136, activation_function=tf.nn.relu)
# add output layer
prediction = add_layer(l1, 136, 10, activation_function=tf.nn.softmax)

# the error between prediction and real data
loss = tf.reduce_mean(tf.reduce_sum(tf.square(ys - prediction),
                     reduction_indices=[1]))
train_step = tf.train.GradientDescentOptimizer(0.1).minimize(loss)

# important step
# tf.initialize_all_variables() no long valid from
# 2017-03-02 if using tensorflow >= 0.12
if int((tf.__version__).split('.')[1]) < 12 and int((tf.__version__).split('.')[0]) < 1:
    init = tf.initialize_all_variables()
else:
    init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)

# plot the real data
fig = plt.figure()
ax = fig.add_subplot(2,1,1)
ax2 = fig.add_subplot(2,1,2)
#ax.scatter(x_data, y_data)
plt.ion()
plt.show()

for i in range(2000):
    # training
    sess.run(train_step, feed_dict={xs: x_data, ys: y_data})
    if i % 50 == 0:
        # to visualize the result and improvement
        try:
            ax.lines.remove(lines[0])
        except Exception:
            pass
        prediction_value = sess.run(prediction, feed_dict={xs: x_data})
        loss_value = sess.run(loss, feed_dict={xs: x_data, ys: y_data})
        # plot the prediction
        #lines = ax.plot(x_data, prediction_value, 'r-', lw=5)
        lines2 = ax2.plot(i, loss_value, 'r*', lw=2)
        plt.pause(0.1)


print(sess.run(loss, feed_dict={xs: x_data, ys: y_data}))

