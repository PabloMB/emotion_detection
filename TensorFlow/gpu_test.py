import tensorflow as tf


##from tensorflow.python.client import device_lib
##print(device_lib.list_local_devices())

print("\nTEST 1")
from tensorflow.python.client import device_lib
print(device_lib.list_local_devices())

print("\nTEST 2")
print(tf.test.gpu_device_name())

print("\nTEST 3")
with tf.device(0):
    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3], name='a')
    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2], name='b')
    c = tf.matmul(a, b)
with tf.Session() as sess:
    print (sess.run(c))

#tensorflow is using the GPU if this program works


                                               
##c = []
##for d in ['/GPU:0', '/GPU:1']:
##  with tf.device(d):
##    a = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[2, 3])
##    b = tf.constant([1.0, 2.0, 3.0, 4.0, 5.0, 6.0], shape=[3, 2])
##    c.append(tf.matmul(a, b))
##with tf.device('/cpu:0'):
##  sum = tf.add_n(c)
### Creates a session with log_device_placement set to True.
##sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
### Runs the op.
##print(sess.run(sum))


