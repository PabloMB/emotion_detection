"""
    A simple neural network written in Keras (TensorFlow backend) to classify the IRIS data
"""

import numpy as np
import tensorflow as tf

from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder

from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

import os
import datetime
import matplotlib.pyplot as plt
##import sys

##parameters
initial_it = 0 #load the model at the iteration saved that is desired
#results
folder = "emotion_iris_results/"
if not os.path.exists(folder):
    os.makedirs(folder)
subfolder = "run6/"
if not os.path.exists(folder+subfolder):
    os.makedirs(folder+subfolder)
model_to_load = "model_"+str(initial_it)
model_h5_to_load = folder+subfolder + model_to_load+".h5"
model_h5_to_save = folder+subfolder + "model_"   #iteration and extension are added later
model_json_to_load = folder+subfolder + model_to_load+".json"
model_json_to_save = folder+subfolder + "model_" #iteration and extension are added later

#databases
#path for IIT PC
filepath = "C:/Users/pmenendezblanco/VisualStudio_projects/superviseddescent_project/superviseddescent_project/results/"+subfolder+"databaseLandmarks3D_full/"
train_filename = filepath + "results_8emotions_normalized_balanced_m1to1_train.csv"
test_filename  = filepath + "results_8emotions_normalized_balanced_m1to1_test.csv"
###path for my PC
##filepath = "D:/Users/Pablo/Documentos/Emotion/superviseddescent_models/"
##train_filename = filepath+subfolder + "results_8emotions_normalized_balanced_train.csv"
##test_filename = filepath+subfolder + "results_8emotions_normalized_balanced_test.csv"
    
#model parameters
inputs = 68*2
outputs = 8
N = inputs*2
A = "tanh"
alpha=0.1 #for LeakyReLu
init = "random_uniform"
bias = "zeros"
D = 0.5
#data parameters
batch_size = 100 #this value is not used but the variable must still exist
batch_percentage = 0.01
#training parameters
learning_rate = 0.001 #better start with 0.01
beta1 = 0.9
num_epochs = 300
##num1 = 400
##change1 = 0.0001
##num2 = 1000
##change2 = 0.00001

if not os.path.exists(train_filename):
    print("train_filename does not exist")
    sys.exit("Error message")
if not os.path.exists(test_filename):
    print("test_filename does not exist")
    sys.exit("Error message")

tf.enable_eager_execution()


def print_current_time():
  print(datetime.datetime.now())

print_current_time()





#Count number of samples for each output
class_samples = [0] * outputs
total_samples = 0
##for i in range(len(class_samples)):
##  class_samples[i] = 0
def count_samples(dataset0): #introduce dataset without batches
  global class_samples
  global total_samples
  class_samples = [0] * outputs
  total_samples = 0
  for features, labels in dataset0:
    #for lab in labels: #this line would work if the dataset had batches
      i = labels.numpy().astype(int)
      class_samples[i] = class_samples[i] + 1
  for i in range(len(class_samples)):
    print("class {} -> {} samples".format(i,class_samples[i]))
    total_samples = total_samples + class_samples[i]
  print("total: {} samples".format(total_samples))




  
##Import and parse the datasets
def parse_csv(line):
  example_defaults = [[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],
                      [0]]  # sets field types
  parsed_line = tf.decode_csv(line, example_defaults)
  # First fields are features, combine into single tensor
  features = tf.reshape(parsed_line[:inputs], shape=(inputs,))
  # Last field is the label
  label = tf.reshape(parsed_line[-1], shape=())
  return features, label

#Load trainset
train_dataset0 = tf.data.TextLineDataset(train_filename)
#train_dataset = train_dataset.skip(1)                      # skip the first header row
train_dataset0 = train_dataset0.map(parse_csv)              # parse each row
train_dataset0 = train_dataset0.shuffle(buffer_size=100000) # randomize
count_samples(train_dataset0) #count total samples
print("total: {} samples".format(total_samples))
train_batch_size = int(total_samples * batch_percentage) #make batches
train_dataset_batched = train_dataset0.batch(total_samples)
print("train batch_size:", train_batch_size)
#train_dataset_batched = train_dataset0.apply(tf.contrib.data.batch_and_drop_remainder(batch_size)) #does not work

# View a single example entry from a batch
features, label = iter(train_dataset_batched).next()
print("example features:", features[0])
print("example label:", label[0])


#Load testset
test_dataset0 = tf.data.TextLineDataset(test_filename)
#test_dataset = test_dataset.skip(1)                      # skip the first header row
test_dataset0 = test_dataset0.map(parse_csv)                # parse each row
test_dataset0 = test_dataset0.shuffle(buffer_size=100000)   # randomize
count_samples(test_dataset0) #count total samples
test_batch_size = int(total_samples * batch_percentage) #make batches
test_dataset_batched = test_dataset0.batch(total_samples)
print("test batch_size:", test_batch_size)
#test_dataset_batched = test_dataset0.apply(tf.contrib.data.batch_and_drop_remainder(batch_size)) #does not work

# View a single example entry from a batch
features, label = iter(test_dataset_batched).next()
print("example features:", features[0])
print("example label:", label[0])






##iris_data = load_iris() # load the iris dataset
##print('Example data: ')
##print(iris_data.data[:5])
##print('Example labels: ')
##print(iris_data.target[:5])
##x = iris_data.data
##y_ = iris_data.target.reshape(-1, 1) # Convert data to a single column

# One Hot encode the class labels
encoder = OneHotEncoder(sparse=False)
##y = encoder.fit_transform(y_)
#print(y)

# Split the data for training and testing
#train_x, test_x, train_y, test_y = train_test_split(x, y, test_size=0.20)

# Build the model

##model = Sequential()
##model.add(Dense(N, input_shape=(inputs,), activation='relu'))
##model.add(Dense(N, activation='relu'))
##model.add(Dense(8, activation='softmax'))

model = tf.keras.Sequential([
  tf.keras.layers.Dense(N, input_shape=(inputs,)),  # input shape required (# of features)
  tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  tf.keras.layers.Dropout(D),

  tf.keras.layers.Dense(2*N),
  tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  tf.keras.layers.Dropout(D),

  tf.keras.layers.Dense(3*N),
  tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  tf.keras.layers.Dropout(D),

##  tf.keras.layers.Dense(4*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(5*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(6*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(7*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(6*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(5*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(4*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),
##
##  tf.keras.layers.Dense(3*N),
##  tf.keras.layers.BatchNormalization(),
##  tf.keras.layers.Activation(A),
##  tf.keras.layers.Dropout(D),

  tf.keras.layers.Dense(2*N),
  tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  tf.keras.layers.Dropout(D),

  tf.keras.layers.Dense(N),
  tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  tf.keras.layers.Dropout(D),
  
  
  tf.keras.layers.Dense(outputs, activation="softmax")
])
##model_softmax = tf.keras.Sequential([
##  tf.keras.layers.Activation("softmax")
##])
#tf.keras.layers.LeakyReLU(alpha=0.1) ? (same with PReLU)
#tf.keras.layers.Dense(N, activation=A, input_shape=(inputs,))
#tf.keras.layers.Dense(N, activation=A),
#tf.keras.layers.Dropout(D)
#tf.keras.layers.BatchNormalization()


##load model if saved before
if os.path.exists(model_json_to_load) and os.path.exists(model_h5_to_load):
  json_file = open(model_json_to_load, 'r')
  loaded_model_json = json_file.read()
  json_file.close()
  model = tf.keras.models.model_from_json(loaded_model_json)
  model.load_weights(model_h5_to_load)
  print("\nMODEL SAVED LOADED\n")
else:
  print("\nMODEL NOT LOADED\n")

##function to save the model
def save_model(model, it):
  model_json = model.to_json()
  f = open(model_json_to_save+str(initial_it+it)+".json", "w")
  f.write(model_json)
  f.close()
  model.save_weights(model_h5_to_save+str(initial_it+it)+".h5")

  f = open(folder+subfolder + 'params.txt','w')
  s = str(batch_size) + "\n" + str(learning_rate) + "\n" + str(num_epochs)
  f.write(s)
  f.close()
  
  print("\nMODEL SAVED\n")

#print layers' weights and biases
#print("layers:",model.layers) gives a very high value
def print_weights_and_biases(model):
  for i in range(3):
    weights, biases = model.layers[i].get_weights()
    print("weights {} : {}".format(i, weights))
    #print("biases: ", biases)
#print_weights_and_biases(model)

#example of outputs
def print_example_outputs(model):
  print("\nexample label:", label[0])
  label_ = model(features)
  print("example prediction:", label_[0])
  ##label_softmax = model_softmax(label_)
  ##print("example softmax prediction:", label_softmax[0])
  print("")




#Calculate accuracy for each class
from sklearn.metrics import classification_report
import numpy as np
##Y_test = np.argmax(y_test, axis=1) # Convert one-hot to index
##y_pred = model.predict_classes(x_test)
y_ = tf.argmax(model(features), axis=1, output_type=tf.int32) #add .numpy()? #Convert one-hot to index
y = label
##print("y_:", tf.size(y_))
##print("y:", tf.size(y))
print(classification_report(y, y_))

##no need yet for this variables to be global
##class_hits = [0] * outputs      #true positives
##class_total = [0] * outputs     #true positives + false positives, to calculate accuracy
##class_accuracy = [0] * outputs  #accuracy
##class_total2 = [0] * outputs    #true positives + false negatives, to calculate recall
##class_recall = [0] * outputs    #recall
##hits = 0
##total = 0
##accuracy = 0
##total2 = 0
##recall = 0
def obtain_accuracies(model,dataset):
  class_hits = [0] * outputs      #true positives
  class_total = [0] * outputs     #true positives + false positives, to calculate accuracy
  class_accuracy = [0] * outputs  #accuracy
  class_total2 = [0] * outputs    #true positives + false negatives, to calculate recall
  class_recall = [0] * outputs    #recall
  hits = 0
  total = 0
  accuracy = 0
  total2 = 0
  recalll = 0
  for x, y in dataset:
    y_ = tf.argmax(model(x), axis=1, output_type=tf.int32)
    length = tf.size(y).numpy().astype(int)
    for i in range(length):
      a = y_[i].numpy().astype(int)
      b = y[i].numpy().astype(int)
      if a==b:
        class_hits[b] = class_hits[b] + 1   #count every time that it is right (true positives)
      class_total[b] = class_total[b] + 1   #count every time that it is emotion b
      class_total2[a] = class_total2[a] + 1 #count every time that it says that it is emotion a 
  for i in range(outputs):
    class_accuracy[i] = float(class_hits[i]) / class_total[i]
    if class_total2[i]==0:
      class_recall[i] = float(0)
    else:
      class_recall[i] = float(class_hits[i]) / class_total2[i]
    hits = hits + class_hits[i]
    total = total + class_total[i]
    total2 = total2 + class_total2[i]
    print("-> emotion {}: accuracy: {:4d} / {:4d} = {:.2f}     |     recall: {:4d} / {:4d} = {:.2f}".format(
      i,class_hits[i],class_total[i],class_accuracy[i],class_hits[i],class_total2[i],class_recall[i]))
  accuracy = float(hits)/total
  recall = float(hits)/total2
  print("total accuracy: {:4d} / {:4d} = {:.2f}     |     total recall: {:4d} / {:4d} = {:.2f}".format(
    hits,total,accuracy,hits,total2,recall))

##print("\nTrain accuracies:")
##obtain_accuracies(model,train_dataset_batched)
##print("\nTest accuracies:")
##obtain_accuracies(model,test_dataset_batched)

  



# Adam optimizer with learning rate of 0.001
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=beta1)
model.compile(optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

print('Neural Network Model Summary: ')
print(model.summary())


# keep results for plotting
train_loss_results = []
train_accuracy_results = []
test_loss_results = []
test_accuracy_results = []
learning_rate_results = []

print("\nTrain accuracies:")
obtain_accuracies(model,train_dataset_batched)
print("\nTest accuracies:")
obtain_accuracies(model,test_dataset_batched)

it = 0
for epoch in range(num_epochs):

    print()
    print_current_time()

    print("epoch: ",epoch)
    
    learning_rate = optimizer._lr #this value doesn't change
    #to calculate leraning_rate (explanation: https://stackoverflow.com/questions/38882593/learning-rate-doesnt-change-for-adamoptimizer-in-tensorflow)
    #a0, bb1, bb2 = optimizer._lr, optimizer._beta1_power.eval(), optimizer._beta2_power.eval()
##    a0 = optimizer._lr
##    bb1 , bb2 = optimizer._get_beta_accumulators()
##    print(bb1)
##    bb1 = bb1.eval()
##    bb2 = bb2.eval()
##    at = a0* (1-bb2)**0.5 /(1-bb1)
    print("learning rate: {}".format(learning_rate))
    learning_rate_results.append(learning_rate)
    
    for x, y in train_dataset_batched:
        #print("size y: ",y.numpy().size)
        yy = encoder.fit_transform(y.numpy().reshape(-1, 1))
        #print("size yy: ",yy.size)
        # Train the model
        model.fit(x, yy, verbose=2, batch_size=train_batch_size, epochs=1)

    print_current_time()
    for x, y in train_dataset_batched:
        yy = encoder.fit_transform(y.numpy().reshape(-1, 1))
        # Test on unseen data
        results = model.evaluate(x, yy, verbose=0)
        print('\t\ttrain loss: {:4f}  accuracy: {:4f}'.format(results[0],results[1]))
        train_loss_results.append(results[0])
        train_accuracy_results.append(results[1])

    print_current_time()
    for x, y in test_dataset_batched:
        #print("size y: ",tf.size(y))
        yy = encoder.fit_transform(y.numpy().reshape(-1, 1))
        #print("size yy: ",tf.size(yy))
        # Test on unseen data
        results = model.evaluate(x, yy, verbose=0)
        print('\t\ttest loss: {:4f}  accuracy: {:4f}'.format(results[0],results[1]))
        test_loss_results.append(results[0])
        test_accuracy_results.append(results[1])
        

    it = it + 1

    if it%25==0:
        print("\nTrain accuracies:")
        obtain_accuracies(model,train_dataset_batched)
        print("\nTest accuracies:")
        obtain_accuracies(model,test_dataset_batched)

    #saving model
    save_model(model,it)

print()
print_current_time()
print("FINISHED")

##Visualize the loss function over time
fig, axes = plt.subplots(4, sharex=True, figsize=(12, 8))
fig.suptitle('Training Metrics')

axes[0].set_ylabel("Train loss", fontsize=14)
axes[0].plot(train_loss_results)

axes[1].set_ylabel("Test loss", fontsize=14)
axes[1].plot(test_loss_results)

axes[2].set_ylabel("Train Accuracy", fontsize=14)
axes[2].plot(train_accuracy_results)

axes[3].set_ylabel("Test Accuracy", fontsize=14)
axes[3].set_xlabel("Epoch", fontsize=14)
axes[3].plot(test_accuracy_results)

plt.show()


print()
print_current_time()
print("Saving results")

#Saving model and results
#save_model(model,it) #already saved in last iteration

resultsfile = folder+subfolder + 'results.csv'
if not os.path.exists(resultsfile):
  f = open(resultsfile, 'w') #create file and write headers
  s = "Train loss" + "," + "Test loss" + "," + "Train accuracy" + "," + "Test accuracy" + "," + "Learning rate" "," + "Batch percentage" + "\n"
  f.write(s)
  f.close()
f = open(resultsfile,'a')
for a,b,c,d,e in zip(train_loss_results,
                     test_loss_results,
                     train_accuracy_results,
                     test_accuracy_results,
                     learning_rate_results):
##    a = a.numpy().astype(float)
##    b = b.numpy().astype(float)
##    c = c.numpy().astype(float)
##    d = d.numpy().astype(float)
    #e = e.numpy().astype(int) #e is a 'float' object
    a = np.asscalar(a)
    b = np.asscalar(b)
    c = np.asscalar(c)
    d = np.asscalar(d)
    s = str(a) + "," + str(b) + "," + str(c) + "," + str(d) + "," + str(e) + "," + str(batch_percentage) + "\n"
    f.write(s)
f.close()


print()
print_current_time()
print("Results saved")
