## A Simple Neural Network in Keras + TensorFlow to classify the Iris Dataset

Following python packages are required to run this file:

```
    pip install tensorflow
    pip install scikit-learn
    pip install keras
```

Then run with: 
```
    $ KERAS_BACKEND=tensorflow python3 iris-keras-nn.py
```