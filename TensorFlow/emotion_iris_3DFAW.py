##Configure imports and eager execution
from __future__ import absolute_import, division, print_function

import os
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow.contrib.eager as tfe
#from sklearn.utils import class_weight
import numpy as np

import datetime
import sys

##parameters
initial_it = 0 #load the model at the iteration saved that is desired
#results
folder = "emotion_iris_3DFAW_results/"
if not os.path.exists(folder):
    os.makedirs(folder)
model_to_load = "model_"+str(initial_it)
model_h5_to_load = folder + model_to_load+".h5"
model_h5_to_save = folder + "model_"   #iteration and extension are added later
model_json_to_load = folder + model_to_load+".json"
model_json_to_save = folder + "model_" #iteration and extension are added later

#databases
#path for IIT PC
filepath = "C:/Users/pmenendezblanco/VisualStudio_projects/Emotion_Detection/VisualStudio/superviseddescent_project/superviseddescent_project/results/3DFAW/"
train_filename = filepath + "results_normalized_train.csv"
test_filename  = filepath + "results_normalized_test.csv"
###path for my PC
##filepath = "D:/Users/Pablo/Documentos/Emotion/superviseddescent_models/"
##train_filename = filepath + "results_8emotions_normalized_balanced_train.csv"
##test_filename = filepath + "results_8emotions_normalized_balanced_test.csv"
    
#model parameters
inputs = 66*2 #x and y
outputs = 66  #z
N = inputs*2
A = "tanh"
alpha=0.1 #for LeakyReLu
init = "random_uniform"
bias = "zeros"
R = 0.1
D1 = 0.3
D2 = 0.5
D3 = 0.7
#data parameters
batch_size = 100 #this value is not used but the variable must still exist
batch_percentage = 0.1
#training parameters
learning_rate = 0.3 #better start with 0.1
beta1 = 0.9
num_epochs = 100
##num1 = 400
##change1 = 0.0001
##num2 = 1000
##change2 = 0.00001
emotions = ["Neutral ", " Happy  ", "  Sad   ", "Surprise", "  Fear  ", "Disgust ", " Anger  ", "Contempt"]


if not os.path.exists(train_filename):
    print("train_filename does not exist")
    sys.exit("Error message")
if not os.path.exists(test_filename):
    print("test_filename does not exist")
    sys.exit("Error message")

tf.enable_eager_execution()

#tf.device(0)

print("TensorFlow version: {}".format(tf.VERSION))
print("Eager execution: {}".format(tf.executing_eagerly()))

def print_current_time():
  print(datetime.datetime.now())

print_current_time()





#Count number of samples for each output
total_samples = 0
##for i in range(len(class_samples)):
##  class_samples[i] = 0
def count_samples(dataset0): #introduce dataset without batches
  global class_samples
  global total_samples
  class_samples = [0] * outputs
  total_samples = 0
  for features, labels in dataset0:
    total_samples = total_samples + 1
  print("total: {} samples".format(total_samples))






##Import and parse the datasets
def parse_csv(line):
  example_defaults = [[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.]]  # sets field types
  parsed_line = tf.decode_csv(line, example_defaults)
  # First fields are features, combine into single tensor
  features = tf.reshape(parsed_line[:inputs], shape=(inputs,))
  # Last field is the label
  label = tf.reshape(parsed_line[inputs:inputs+outputs], shape=(outputs,))
  return features, label

#Load trainset
train_dataset0 = tf.data.TextLineDataset(train_filename)
#train_dataset = train_dataset.skip(1)                      # skip the first header row
train_dataset0 = train_dataset0.map(parse_csv)              # parse each row
train_dataset0 = train_dataset0.shuffle(buffer_size=1000) # randomize
count_samples(train_dataset0) #count total samples
print("total: {} samples".format(total_samples))
batch_size = int(total_samples * batch_percentage) #make batches
train_dataset_batched = train_dataset0.batch(batch_size)
print("train batch_size:", batch_size)
#train_dataset_batched = train_dataset0.apply(tf.contrib.data.batch_and_drop_remainder(batch_size)) #does not work

# View a single example entry from a batch
features, label = iter(train_dataset_batched).next()
print("example features:", features[0])
print("example label:", label[0])


#Load testset
test_dataset0 = tf.data.TextLineDataset(test_filename)
#test_dataset = test_dataset.skip(1)                      # skip the first header row
test_dataset0 = test_dataset0.map(parse_csv)                # parse each row
test_dataset0 = test_dataset0.shuffle(buffer_size=1000)   # randomize
count_samples(test_dataset0) #count total samples
batch_size = int(total_samples * batch_percentage) #make batches
test_dataset_batched = test_dataset0.batch(batch_size)
print("test batch_size:", batch_size)
#test_dataset_batched = test_dataset0.apply(tf.contrib.data.batch_and_drop_remainder(batch_size)) #does not work

# View a single example entry from a batch
features, label = iter(test_dataset_batched).next()
print("example features:", features[0])
print("example label:", label[0])




regularizer = tf.contrib.layers.l2_regularizer(scale=R)

#Select the type of model
model = tf.keras.Sequential([
  tf.keras.layers.Dense(N, input_shape=(inputs,)),  # input shape required (# of features)
  #tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  #tf.keras.layers.Dropout(D1),

  tf.keras.layers.Dense(2*N),
  #tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  #tf.keras.layers.Dropout(D2),

  tf.keras.layers.Dense(3*N),
  #tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  #tf.keras.layers.Dropout(D3),

  tf.keras.layers.Dense(2*N),
  #tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  #tf.keras.layers.Dropout(D2),

  tf.keras.layers.Dense(N),
  #tf.keras.layers.BatchNormalization(),
  tf.keras.layers.Activation(A),
  #tf.keras.layers.Dropout(D1),
  
  
  tf.keras.layers.Dense(outputs)
])
##model_softmax = tf.keras.Sequential([
##  tf.keras.layers.Activation("softmax")
##])
#tf.keras.layers.LeakyReLU(alpha=0.1) ? (same with PReLU)
#tf.keras.layers.Dense(N, activation=A, input_shape=(inputs,))
#tf.keras.layers.Dense(N, activation=A),
#regularizer = tf.contrib.layers.l2_regularizer(scale=R)
#tf.keras.layers.Dense(N,kernel_regularizer=regularizer, bias_regularizer=regularizer),
#tf.keras.layers.Dropout(D)
#tf.keras.layers.BatchNormalization()

##load model if saved before
if os.path.exists(model_json_to_load) and os.path.exists(model_h5_to_load):
  json_file = open(model_json_to_load, 'r')
  loaded_model_json = json_file.read()
  json_file.close()
  model = tf.keras.models.model_from_json(loaded_model_json)
  model.load_weights(model_h5_to_load)
  print("\nMODEL SAVED LOADED\n")
else:
  print("\nMODEL NOT LOADED\n")

##function to save the model
def save_model(model, it):
  model_json = model.to_json()
  f = open(model_json_to_save+str(initial_it+it)+".json", "w")
  f.write(model_json)
  f.close()
  model.save_weights(model_h5_to_save+str(initial_it+it)+".h5")
  f = open(folder + 'params.txt','w')
  s = str(batch_size) + "\n" + str(learning_rate) + "\n" + str(num_epochs)
  f.write(s)
  f.close()
  
  print("\nMODEL SAVED\n")

#print layers' weights and biases
#print("layers:",model.layers) gives a very high value
def print_weights_and_biases(model):
  for i in range(3):
    weights, biases = model.layers[i].get_weights()
    print("weights {} : {}".format(i, weights))
    #print("biases: ", biases)
#print_weights_and_biases(model)

#example of outputs
def print_example_outputs(model):
  print("\nexample label:", label[0])
  label_ = model(features)
  print("example prediction:", label_[0])
  ##label_softmax = model_softmax(label_)
  ##print("example softmax prediction:", label_softmax[0])
  print("")




#Train the model
def loss(model, x, y):
  y_ = model(x)
  return tf.losses.absolute_difference(labels=y, predictions=y_)
  #return tf.losses.mean_squared_error(labels=y, predictions=y_)

def grad(model, inputs, targets):
  with tf.GradientTape() as tape:
    loss_value = loss(model, inputs, targets)
  return tape.gradient(loss_value, model.variables)




###Calculate accuracy for each class
##from sklearn.metrics import classification_report
##import numpy as np
####Y_test = np.argmax(y_test, axis=1) # Convert one-hot to index
####y_pred = model.predict_classes(x_test)
##y_ = tf.argmax(model(features), axis=1, output_type=tf.int32) #add .numpy()? #Convert one-hot to index
##y = label
####print("y_:", tf.size(y_))
####print("y:", tf.size(y))
##print(classification_report(y, y_))




def obtain_losses(model,dataset_batched,show,it):
  each_error = [0] * outputs
  count = 0
  total_error = 0 #actually, squared error
  for x, y in dataset_batched:
      y_ = model(x)
      for y1_,y1 in zip(y_,y):
        for i in range(outputs):
          each_error[i] = each_error[i] + abs(y1_[i]-y1[i])#**2
        count = count + 1
  for i in range(outputs):
    each_error[i] = each_error[i] / count
    #print(" output {} -> error: {:3f}".format(i,each_error[i]))
    total_error = total_error + each_error[i]
  total_error = total_error / outputs
  #print(" total mean error: {:3f}".format(total_error))

  if show:
    fig, axes = plt.subplots(1, sharex=True, figsize=(12, 8))
    fig.suptitle('Errors Metrics')

    axes.set_ylabel("Error", fontsize=14)
    axes.plot(each_error)
    axes.set_xlabel("Output", fontsize=14)

    plt.show()

  resultsfile = folder + 'results.txt'
  f = open(model_json_to_save+str(initial_it+it)+".csv", "w")
  f.close()
  f = open(model_json_to_save+str(initial_it+it)+".csv", "a")
  for i in range(outputs):
    a = each_error[i].numpy().astype(float)
    a = np.asscalar(a)
    s = str(a)
    if i!=(outputs-1):
      s = s + ","
    f.write(s)
  f.close()


print_example_outputs(model)
obtain_losses(model,test_dataset_batched,False,0)



optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
#optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=beta1)
# Note: Rerunning this cell uses the same model variables

# keep results for plotting
train_loss_results = []
test_loss_results = []
learning_rate_results = []

##print("\nTrain accuracies:")
##obtain_accuracies(model,train_dataset_batched)
##print("\nTest accuracies:")
##obtain_accuracies(model,test_dataset_batched)
    
it = 0
for epoch in range(num_epochs):
  train_loss_avg = tfe.metrics.Mean()

##  if epoch == num1:
##    learning_rate = change1
##    optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
##
##  if epoch == num2:
##    learning_rate = change2
##    optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)

  print()
  print_current_time()
  
  #learning_rate = optimizer._lr
  print("learning rate: {}".format(learning_rate))
  learning_rate_results.append(learning_rate)
  
  # Training loop - using batches
  i = 0
  for x, y in train_dataset_batched:
    i = i + 1
    # Optimize the model
    grads = grad(model, x, y)
    #print("gradients",grads)
    optimizer.apply_gradients(zip(grads, model.variables),
                              global_step=tf.train.get_or_create_global_step())
                                  
    # Track progress
    train_loss_avg(loss(model, x, y))  # add current batch loss

  train_loss_results.append(train_loss_avg.result())
  print("Epoch {:03d}: step {:04d}:      Loss: {:.3f}"
        .format(epoch,i,train_loss_avg.result()))

  #Obtaining test accuracy
  j = 0
  test_loss_avg = tfe.metrics.Mean()
  for (x, y) in test_dataset_batched:
    j = j + 1
    test_loss_avg(loss(model, x, y))
  test_loss_results.append(test_loss_avg.result())
  print_current_time()
  print("Epoch {:03d}: step {:04d}:                         Loss: {:.3f}"
        .format(epoch,j,test_loss_avg.result()))
  
  print_example_outputs(model)

  it = it + 1
  
##  #if it%25==0:
##  print("\nTrain accuracies:")
##  obtain_accuracies(model,train_dataset_batched)
##  print("\nTest accuracies:")
##  obtain_accuracies(model,test_dataset_batched)

  #saving model
  save_model(model,it)
  obtain_losses(model,test_dataset_batched,False,it)



##Visualize the loss function over time
fig, axes = plt.subplots(2, sharex=True, figsize=(12, 8))
fig.suptitle('Training Metrics')

axes[0].set_ylabel("Train loss", fontsize=14)
axes[0].plot(train_loss_results)

axes[1].set_ylabel("Test loss", fontsize=14)
axes[1].plot(test_loss_results)
axes[1].set_xlabel("Epoch", fontsize=14)

plt.show()



###Evaluate the model's effectiveness
##test_accuracy = tfe.metrics.Accuracy()
##for (x, y) in test_dataset_batched:
##  prediction = tf.argmax(model(x), axis=1, output_type=tf.int32)
##  test_accuracy(prediction, y)
##print_current_time()
##print("Test set accuracy: {:.3%}".format(test_accuracy.result()))



#Saving model and results
#save_model(model,it) #already saved in last iteration

resultsfile = folder + 'results.csv'
if not os.path.exists(resultsfile):
  f = open(resultsfile, 'w') #create file and write headers
  s = "Train loss" + "," + "Test loss" + "," + "Learning rate" "," + "Batch percentage" + "\n"
  f.write(s)
  f.close()
f = open(resultsfile,'a')
for a,b,e in zip(train_loss_results,
                 test_loss_results,
                 learning_rate_results):
    a = a.numpy().astype(float)
    b = b.numpy().astype(float)
    #e = e.numpy().astype(int) #e is a 'float' object
    a = np.asscalar(a)
    b = np.asscalar(b)
    s = str(a) + "," + str(b) + "," + str(e) + "," + str(batch_percentage) + "\n"
    f.write(s)
f.close()

