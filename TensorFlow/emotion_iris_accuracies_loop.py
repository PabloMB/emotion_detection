##Configure imports and eager execution
from __future__ import absolute_import, division, print_function

import os
import matplotlib.pyplot as plt

import tensorflow as tf
import tensorflow.contrib.eager as tfe

import datetime


##parameters
initial_it = 1 #load the model at the iteration saved that is desired
jump = 25 #number of saved files to skip + 1
#results
folder = "emotion_iris_results/"
subfolder = "run6/"
if not os.path.exists(folder):
    os.makedirs(folder)
if not os.path.exists(folder+subfolder):
    os.makedirs(folder+subfolder)
##model_to_load = "model_"+str(initial_it)
##model_h5_to_load = folder+subfolder + model_to_load+".h5"
##model_h5_to_save = folder+subfolder + "model_"   #iteration and extension are added later
##model_json_to_load = folder+subfolder + model_to_load+".json"
##model_json_to_save = folder+subfolder + "model_" #iteration and extension are added later

#databases
#path for IIT PC
filepath = "C:/Users/pmenendezblanco/VisualStudio_projects/superviseddescent_project/superviseddescent_project/results/"+subfolder+"databaseLandmarks3D_made_full/"
train_filename = filepath + "results_8emotions_normalized_duplicated_balanced_m1to1_train.csv"
test_filename  = filepath + "results_8emotions_normalized_duplicated_balanced_m1to1_test.csv"
###path for my PC
##filepath = "D:/Users/Pablo/Documentos/Emotion/superviseddescent_models/"
##train_filename = filepath+subfolder + "results_8emotions_normalized_balanced_train.csv"
##test_filename = filepath+subfolder + "results_8emotions_normalized_balanced_test.csv"

###model parameters
inputs = 68*2
##outputs = 8
##N = inputs*2
##A = "relu"
##init = "random_uniform"
##bias = "zeros"
##D = 0.25
###data parameters
batch_size = 100
###training parameters
##learning_rate = 0.01
##num_epochs = 2200



tf.enable_eager_execution()

print("TensorFlow version: {}".format(tf.VERSION))
print("Eager execution: {}".format(tf.executing_eagerly()))

def print_current_time():
  print(datetime.datetime.now())

print_current_time()


##Import and parse the datasets

def parse_csv(line):
  example_defaults = [[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],[0.],
                      [0.],[0.],[0.],[0.],
                      [0]]  # sets field types
  parsed_line = tf.decode_csv(line, example_defaults)
  # First 4 fields are features, combine into single tensor
  features = tf.reshape(parsed_line[:inputs], shape=(inputs,))
  # Last field is the label
  label = tf.reshape(parsed_line[-1], shape=())
  return features, label

#Load trainset
train_dataset = tf.data.TextLineDataset(train_filename)
#train_dataset = train_dataset.skip(1)                      # skip the first header row
train_dataset = train_dataset.map(parse_csv)                # parse each row
train_dataset = train_dataset.shuffle(buffer_size=100000)   # randomize
train_dataset = train_dataset.batch(batch_size)

# View a single example entry from a batch
features, label = iter(train_dataset).next()
print("example features:", features[0])
print("example label:", label[0])

#example features: tf.Tensor([6.  2.7 5.1 1.6], shape=(4,), dtype=float32)
#example label: tf.Tensor(1, shape=(), dtype=int32)

#Load trainset
test_dataset = tf.data.TextLineDataset(test_filename)
#test_dataset = test_dataset.skip(1)                      # skip the first header row
test_dataset = test_dataset.map(parse_csv)                # parse each row
test_dataset = test_dataset.shuffle(buffer_size=100000)   # randomize
test_dataset = test_dataset.batch(batch_size)

# View a single example entry from a batch
features, label = iter(test_dataset).next()
print("example features:", features[0])
print("example label:", label[0])
print("label size:", tf.size(label))







###Select the type of model
##model = tf.keras.Sequential([
##  tf.keras.layers.Dense(N, activation=A, input_shape=(inputs,)),  # input shape required (# of features)
##  tf.keras.layers.Dense(N, activation=A),
##  tf.keras.layers.Dense(N, activation=A),
##  tf.keras.layers.Dense(N, activation=A),
##  tf.keras.layers.Dense(outputs)
##])


##load model if saved before
def load_model(it):
  model_to_load = "model_"+str(it)
  model_h5_to_load = folder+subfolder + model_to_load+".h5"
  model_json_to_load = folder+subfolder + model_to_load+".json"
  if os.path.exists(model_json_to_load) and os.path.exists(model_h5_to_load):
    json_file = open(model_json_to_load, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = tf.keras.models.model_from_json(loaded_model_json)
    model.load_weights(model_h5_to_load)
    print("\nModel saved loaded\n")
  else:
    model = None
    print("\nModel not loaded\n")
  return model

##function to save the model
def save_model(model, it):
  model_json = model.to_json()
  f = open(model_json_to_save+str(initial_it+it)+".json", "w")
  f.write(model_json)
  f.close()
  model.save_weights(model_h5_to_save+str(initial_it+it)+".h5")

  f = open(folder+subfolder + 'params.txt','w')
  s = str(batch_size) + "\n" + str(learning_rate) + "\n" + str(num_epochs)
  f.write(s)
  f.close()
  
  print("\nMODEL SAVED\n")


#Train the model
def loss(model, x, y):
  y_ = model(x)
  return tf.losses.sparse_softmax_cross_entropy(labels=y, logits=y_)

def grad(model, inputs, targets):
  with tf.GradientTape() as tape:
    loss_value = loss(model, inputs, targets)
  return tape.gradient(loss_value, model.variables)

# keep results for plotting
train_loss_results = []
train_accuracy_results = []
test_loss_results = []
test_accuracy_results = []
learning_rate_results = []

it = initial_it
while True:
  train_loss_avg = tfe.metrics.Mean()
  epoch_accuracy = tfe.metrics.Accuracy()

  model = load_model(it)

  if model==None:
    break
  
  # Training loop - using batches
  i = 0
  for x, y in train_dataset:
    i = i + 1
    # Track progress
    train_loss_avg(loss(model, x, y))  # add current batch loss
    # compare predicted label to actual label
    epoch_accuracy(tf.argmax(model(x), axis=1, output_type=tf.int32), y)

  train_loss_results.append(train_loss_avg.result())
  train_accuracy_results.append(epoch_accuracy.result())
  print_current_time()
  print("Epoch {:03d}: step {:04d}:      Loss: {:.3f}, Accuracy: {:.3%}"
        .format(it,i,train_loss_avg.result(),epoch_accuracy.result()))

  #Obtaining test accuracy
  j = 0
  test_loss_avg = tfe.metrics.Mean()
  test_accuracy = tfe.metrics.Accuracy()
  for (x, y) in test_dataset:
    j = j + 1
    prediction = tf.argmax(model(x), axis=1, output_type=tf.int32)
    test_loss_avg(loss(model, x, y))
    test_accuracy(prediction, y)
  test_loss_results.append(test_loss_avg.result())
  test_accuracy_results.append(test_accuracy.result())
  print_current_time()
  print("Epoch {:03d}: step {:04d}:                                 Loss: {:.3f}, Accuracy: {:.3%}"
        .format(it,j,test_loss_avg.result(),test_accuracy.result()))

  #saving model
  it = it + jump



##Visualize the loss function over time
fig, axes = plt.subplots(4, sharex=True, figsize=(12, 8))
fig.suptitle('Training Metrics')

axes[0].set_ylabel("Train loss", fontsize=14)
axes[0].plot(train_loss_results)

axes[1].set_ylabel("Test loss", fontsize=14)
axes[1].plot(test_loss_results)

axes[2].set_ylabel("Train Accuracy", fontsize=14)
axes[2].plot(train_accuracy_results)

axes[3].set_ylabel("Test Accuracy", fontsize=14)
axes[3].set_xlabel("Epoch", fontsize=14)
axes[3].plot(test_accuracy_results)

plt.show()



#Saving results
resultsfile = folder+subfolder + 'results.csv'
f = open(resultsfile,'w')
for a,b,c,d in zip(train_loss_results,
                     test_loss_results,
                     train_accuracy_results,
                     test_accuracy_results):
    s = str(tf.to_float(a)) + "," + str(tf.to_float(b)) + "," + str(tf.to_float(c)) + "," + str(tf.to_float(d)) + "\n"
    f.write(s)
f.close()

