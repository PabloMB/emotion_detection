#include "keras_model.h"

#include <iostream>
#include <cmath>

using namespace std;
using namespace keras;

// Step 1
// Dump keras model and input sample into text files
// python dump_to_simple_cpp.py -a example/my_nn_arch.json -w example/my_nn_weights.h5 -o example/dumped.nnet
// Step 2
// Use text files in c++ example. To compile:
// g++ keras_model.cc example_main.cc
// To execute:
// a.out

int main() {
  cout << "This is simple example with Keras neural network model loading into C++.\n"
           << "Keras model will be used in C++ for prediction only." << endl;

  DataChunk *sample = new DataChunkFlat();

  cout << "\nReading data" << endl;
  sample->read_from_file("./DNN1/sample.dat");
  std::cout << sample->get_1d().size() << std::endl;

  cout << "\nReading neural network structure" << endl;
  KerasModel m("./DNN1/model_7000_89-80.nnet", true);

  cout << "\nComputing output" << endl;
  std::vector<float> out = m.compute_output(sample);
  delete sample;

  softmax(out);
  show_values(out);

  system("pause");
  return 0;
}
