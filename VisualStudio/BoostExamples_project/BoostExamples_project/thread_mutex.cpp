#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::to_string;

#define THREAD_COUNT 4

void wait(int seconds)
{
	boost::this_thread::sleep_for(boost::chrono::seconds{ seconds });
}

boost::mutex mutex;

void thread()
{
	using boost::this_thread::get_id;
	for (int i = 0; i < 5; ++i)
	{
		wait(1);
		mutex.lock(); //prevents other thread from using cout while this thread is using it
		cout << "Thread " << boost::this_thread::get_id() << ": " << i << std::endl;
		mutex.unlock();
	}
}

void long_sum() {
	unsigned long int total = 0;
	unsigned long int end = total - 1;
	string text_end = "end: " + to_string(end) + "\n";
	cout << text_end;
	for (int i = 1; i < end; ++i)
		total += i;
	string text_to_print = "Thread " + to_string(boost::this_thread::get_id()) + "\tsum: " + to_string(total) + "\n";
	cout << text_to_print;
}

int main()
{
	boost::thread *threads[THREAD_COUNT];
	
	// Creation
	for (int i = 0; i < THREAD_COUNT; i++) {
		threads[i] = new boost::thread(long_sum);
	}

	// Cleanup
	for (int i = 0; i < THREAD_COUNT; i++) {
		threads[i]->join();
		delete threads[i];
	}

	system("pause");
}