#include <iostream>
#include <boost/thread.hpp>

using namespace std;
using boost::thread;
using boost::mutex;

mutex a;

const int THREAD_COUNT = 10;

void worker(int i) {
    a.lock();
    cout << "Worker thread " << i << endl;
    a.unlock();
}

int main(int argc, char** argv)
{
    thread *threads[THREAD_COUNT];

    // Creation
    for(int i = 0; i < THREAD_COUNT; i++) {
        threads[i] = new thread(worker, i);
    }

    // Cleanup
    for(int i = 0; i < THREAD_COUNT; i++) {
        threads[i]->join();
        delete threads[i];
    }

	system("pause");
    return 0;
}

