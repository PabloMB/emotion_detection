To build projects from scratch in Visual Studio:
-create the project
-add the code
-go to Project > Properties
-make sure to be in the desired configuration (Debug or Release; x64 or other)
-in C/C++ > General > Additional Include Directories: add the path to each library (this will allow the project to compile)
-if Visual Studio created precompiled headers, you might not need them and might need to remove/delete these file to compile correctly (also go to C/C++ > Precompiled Headers > Precompiled Header and select Not using precompiled headers)
-in Linker > General > Additional Library Directories: add the path to each folder where the .lib files of the libraries are (this will allow the project to link)
-in Linker > Input > Additional Dependencies: add the name of each .lib file necessary (this will allow the project to link)


In some cases, you might need to add one or more of the following line to some cpp files:
#define _CRT_SECURE_NO_WARNINGS  //to avoid error C4996 with localtime (unsafe function)
#define D_SCL_SECURE_NO_WARNINGS

You will need to replace the source.cpp file of the project with the one inside the dlib folder:
-right click on the source.cpp file of the project inside Visual Studio and select remove
-right click on Source files > Add > Existing Item... > Go to C:\dlib-19.10\source\dlib\all and select the source.cpp file

-when running the project, it may require the file opencv_world341.dll. Copy it from the opencv library and paste it inside x64/Release. (Note: Visual Studio creates two x64 folder, the right one for this is in the root folder of the project)

If any other problem occurs, try to find the solution on google, as many are already solved on the Internet.



To open one of the projects in this folder:
-double click the .sln file (or right click > open with > Microsoft Visual Studio)
-change the path to where you have installed the big libraries (OpenCV, dlib and Boost) in:
Project > Properties > C/C++ > General > Additional Include Directories
Project > Properties > Linker > General > Additional Library Directories
(make sure to be in the desired configuration (Debug or Release; x64 or other))
-make sure that the names of the .lib files are correct in Linker > Input > Additional Dependencies (in case you built the libraries for a different Visual Studio)



All these projects have been built with Visual Studio 2017 Community, in Release x64
Note: you might need to correct the path to the databases, since your location will be different




How to build the libraries

OPENCV:

Already built in the Win pack provided in the download page of OpenCV


DLIB:

-Download dlib
-Extract dlib
-Create "source" and "build" folders and insert all the original folders inside "source" folder
-Open CMake gui
-Add path to source and build. Example:
Where is the source code: C:\dlib-19.15\source
Where to build the binaries: C:\dlib-19.15\build
-Configure
-Generate
-Open .sln file with Visual Studio
-Build in Debug and Release mode


BOOST:

To build Boost: https://stackoverflow.com/questions/13042561/fatal-error-lnk1104-cannot-open-file-libboost-system-vc110-mt-gd-1-51-lib/13042696?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

To build Boost on Windows:
https://stackoverflow.com/questions/41464356/build-boost-with-msvc-14-1-vs2017-rc
Boost 1.63 doesn't fully support VS2017, but you can trick it to find VC++2017 compiler:
1.	Run bootstrap.bat in boost directory
2.	Update the project-config.jam to include: using msvc : 14.0 : <path to x86 or x64 cl.exe>. Should be something like "c:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Tools\MSVC\14.10.24911\bin\HostX64\x64\cl.exe"
3.	Run "Developer Command Prompt for VS 2017 RC" from Windows Start Menu to boostrap from a shell configured using the x86 vcvars or x64 vcvars.
4.	Run b2 toolset=msvc-14.0 in that command prompt. For the x64 build, add address-model=64to the b2 command line.

UPDATE: Boost 1.64 should support VS2017
Run "x86 Native Tools Command Prompt for VS 2017" or "x64 Native Tools Command Prompt for VS 2017" from Start Menu, then inside command prompt run b2:
32-bit: b2 toolset=msvc-14.1 --build-dir=.x86 --stagedir=stage_x86
64-bit: b2 toolset=msvc-14.1 address-model=64 --build-dir=.x64 --stagedir=stage_x64
Add link=shared to build shared libraries

Or use built binaries: https://sourceforge.net/projects/boost/files/boost-binaries/


LIBJPEG TURBO

-download and extract vcpkg: https://stackoverflow.com/questions/48740107/link-libjpeg-turbo-in-vs-c-2017?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
-open the command window or powershell inside the folder
-run: .\vcpkg install libjpeg-turbo:x64-windows-static
-run: .\vcpkg integrate install


