#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define D_SCL_SECURE_NO_WARNINGS

#include <opencv2/opencv.hpp>
#include <string>
#include <dlib/geometry/rectangle.h>
using std::string;

#define MAXCOLS 1000
#define MAXROWS 1000

void myresize(cv::Mat& image);

void print_local_time();

void print_time(unsigned long int time);

void get_params(const string& paramsfile, string* params, const int num);

void get_params(const string& paramsfile, string& folder, string& modelfile);

void get_training_params(const string& paramsfile, string& selection, string& meanfile, string& configfile, string& evaluationfile, string& outputmodel, string& outputerrors, string& outputparams);

//transforming from dlib rectangle to opencv rect
cv::Rect dlibRectangleToOpenCV(dlib::rectangle r);
//transforming opencv rect to dlib rectangle
dlib::rectangle openCVRectToDlib(cv::Rect r);
