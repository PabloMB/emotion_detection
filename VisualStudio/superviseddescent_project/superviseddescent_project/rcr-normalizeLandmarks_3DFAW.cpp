
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>     /* srand, rand */

#include "utils.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;

#define LANDMARKS_NUM 66
#define PI   3.1415926535897932384626433832795
#define PRINT_EVERY_N 10
bool duplicate = false;

void get_data_from_csv(const string filename, vector<vector<string>>& matrix) {
	std::ifstream inputfile(filename);
	string current_line;
	matrix.clear();

	while (getline(inputfile, current_line)) {
		vector<string> vect;
		std::stringstream temp(current_line);
		string single_value;
		while (getline(temp, single_value, ',')) {
			vect.push_back(single_value);
		}
		matrix.push_back(vect);
	}
};

//repeated in rcr-normalizeLandmarks...
void write_csv(const string filename, const vector<vector<string>>& matrix)
{
	std::ofstream myfile;
	myfile.open(filename.c_str());
	

	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {

			for (int j = 0; j < matrix[i].size(); j++) {
				myfile << matrix[i][j];
				if (j != matrix[i].size() - 1)
					myfile << ",";
			}
			myfile << endl;

		}
	}

	myfile.close();
}

void duplicate_samples(vector<vector<string>>& matrix)
{
	int original_size = matrix.size();
	for (int i = 0; i < original_size; i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			matrix.push_back(matrix[i]);

			for (int j = 0; j < LANDMARKS_NUM; j++) { //invert x coordinates
				double value = stod(matrix[i][j]);
				value *= -1; //results approximately symmetrical at 0
				matrix[i][j] = std::to_string(value);
			}

			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
		}
	}
}

void normalize_angles(vector<string>& vector)
{
	//scale all angles to be between 0 and 1 (initially they are between -2PI and 2PI )
	for (int j = 0; j < 3; j++) {
		int index = LANDMARKS_NUM * 2 + j;
		double value = stod(vector[index]);
		//cout << value << " to ";
		value = (value + PI) / (2*PI);
		//cout << value << endl;
		vector[index] = std::to_string(value);
	}

}

void normalize_angles(vector<vector<string>>& matrix)
{
	for (int i = 0; i < matrix.size(); i++) {
		if (matrix[i].size() >= LANDMARKS_NUM * 2) {
			normalize_angles(matrix[i]);
			if (i % PRINT_EVERY_N == 0)
				cout << "line: " << i << endl;
		}
	}
}

void divide_data(vector<vector<string>>& matrix_origin, vector<vector<string>>& matrix_train, vector<vector<string>>& matrix_test, float separation) {
	int num_train = matrix_origin.size()*separation;
	int num_test = matrix_origin.size() - num_train;
	int count_train = 0;
	int count_test = 0;
	srand (time(NULL));

	if (separation > 1 || separation < 0) {
		cout << "Separation must be between 0 and 1. Setting separation to 0.5" << endl;
		separation = 0.5;
	}
	for (int i = 0; i < matrix_origin.size(); i++) {
		if (i % PRINT_EVERY_N == 0)
			cout << "line: " << i << endl;

		float prob = rand() % 101 / 100;

		if (prob <= separation && count_train < num_train) {
			matrix_train.push_back(matrix_origin[i]);
			count_train++;
		}
		else {
			if (count_test < num_test) {
				matrix_test.push_back(matrix_origin[i]);
				count_test++;
			}
			else {
				matrix_train.push_back(matrix_origin[i]);
				count_train++;
			}

		}
	}
}

int main(int argc, char *argv[])
{
	string paramsfile = "config_normalizeLandmarks_3DFAW.txt";

	string filefolder;
	string* params = new string[1];
	get_params(paramsfile, params, 1);
	filefolder = params[0];
	delete[] params;
	cout << "file folder: " << filefolder << endl;


	//get data from original csv file
	cout << "\ngetting data" << endl;
	string filename = "results_normalized";
	vector<vector<string>> csvdata;
	get_data_from_csv(filefolder + filename + ".csv", csvdata);

	//keep only the emotions we want
	cout << "\nkeep only the emotions we want" << endl;

	//duplicate sample
	if (duplicate) {
		cout << "\nduplicating samples" << endl;
		duplicate_samples(csvdata);
	}

	////normalize angles
	//cout << "\nnormalizing angles" << endl;
	//normalize_angles(csvdata8);

	//adding ones
	//cout << "\nadding ones" << endl;
	//add_ones(csvdata8);

	//randomly divide in two
	cout << "\ndividing dataset" << endl;
	vector<vector<string>> csvdata_train;
	vector<vector<string>> csvdata_test;
	divide_data(csvdata, csvdata_train, csvdata_test, 0.8);

	//save data to new csv file
	cout << "\nsaving data" << endl;
	if (duplicate)
		filename = filename + "_duplicated";
	//write_csv(modelfolder + modelsubfolder + name + "_all.csv", csvdataN);
	write_csv(filefolder + filename + "_train.csv", csvdata_train);
	write_csv(filefolder + filename + "_test.csv", csvdata_test);


	system("pause");
	return EXIT_SUCCESS;

}