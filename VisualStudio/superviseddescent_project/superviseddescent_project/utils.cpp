#include "utils.h"

void myresize(cv::Mat& image) {
	if (image.cols > MAXCOLS)
		cv::resize(image, image, cv::Size(MAXCOLS, image.rows * MAXCOLS / image.cols), 0, 0, cv::INTER_LINEAR); //cols,rows
	if (image.rows > MAXROWS)
		cv::resize(image, image, cv::Size(image.cols * MAXROWS / image.rows, MAXROWS), 0, 0, cv::INTER_LINEAR);
}

void print_local_time() {
	std::time_t t = std::time(0);
	char cstr[15];
	std::strftime(cstr, sizeof(cstr), "%I:%M:%S%p", std::localtime(&t)); //complete: "%I:%M:%S %p %Z"
	std::cout << cstr << "\t";
}

void print_time(unsigned long int time) {
	int millis = time % 1000; //get milliseconds
	time /= 1000; //change time to seconds
	int seconds = time % 60; //get seconds
	time /= 60; //change time to minutes
	int minutes = time % 60; //get minutes
	time /= 60; //change time to hours
	int hours = time; //get hours

	std::cout << hours << "h ";
	std::cout << minutes << "min ";
	std::cout << seconds << "s ";
	std::cout << millis << "ms ";
	std::cout << std::endl;
}

void get_params(const string& paramsfile, string* params, const int num)
{
	std::ifstream fin;
	fin.open(paramsfile, std::ifstream::in);
	
	for (int i = 0; i < num; i++) {
		fin >> params[i];
	}
}

void get_params(const string& paramsfile, string& folder, string& modelfile)
{
	std::ifstream fin;
	fin.open(paramsfile, std::ifstream::in);
	fin >> folder;
	fin >> modelfile;
}

void get_training_params(const string& paramsfile, string& selection, string& meanfile, string& configfile, string& evaluationfile, string& outputmodel, string& outputerrors, string& outputparams)
{
	std::ifstream fin;
	fin.open(paramsfile, std::ifstream::in);
	fin >> selection;
	fin >> meanfile;
	fin >> configfile;
	fin >> evaluationfile;
	fin >> outputmodel;
	fin >> outputerrors;
	fin >> outputparams;
}

//transforming from dlib rectangle to opencv rect
cv::Rect dlibRectangleToOpenCV(dlib::rectangle r)
{
	return cv::Rect(cv::Point2i(r.left(), r.top()), cv::Point2i(r.right() + 1, r.bottom() + 1));
}

//transforming opencv rect to dlib rectangle
dlib::rectangle openCVRectToDlib(cv::Rect r)
{
	return dlib::rectangle((long)r.tl().x, (long)r.tl().y, (long)r.br().x - 1, (long)r.br().y - 1);
}
