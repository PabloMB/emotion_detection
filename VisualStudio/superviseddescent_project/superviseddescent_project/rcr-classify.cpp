/*
 * superviseddescent: A C++11 implementation of the supervised descent
 *                    optimisation method
 * File: apps/rcr/rcr-detect.cpp
 *
 * Copyright 2015 Patrik Huber
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define EMOTION_TO_FIND 4 //-1 for all emotions

#define SHOW_PROCESSED_IMAGE true

#define PRINT_MSGS true

#define SAVE_PROCESSED_IMAGE false

#define SAVE_EVERY_N_CYCLES 1000
unsigned long int save_every_n_cycles = SAVE_EVERY_N_CYCLES;

#define LIMIT_LOAD_FOR_DEBUGGING false
#define MAX_NUM_FOLDERS_LOAD 100
#define MAX_NUM_FILES_LOAD 1000

#define _CRT_SECURE_NO_WARNINGS  //to avoid error C4996 with localtime (unsafe function)

#include "helpers.hpp"

#include "superviseddescent/superviseddescent.hpp"
#include "superviseddescent/regressors.hpp"

#include "rcr/landmarks_io.hpp"
#include "rcr/model.hpp"

#include "cereal/cereal.hpp"

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/quaternion.hpp"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/objdetect/objdetect.hpp"

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include "boost/program_options.hpp"
#include "boost/filesystem.hpp"
//#include <boost/thread.hpp>
#include "boost/algorithm/string.hpp"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/info_parser.hpp"

#include "Eigen/Core"
#include "eos/core/Image.hpp"
#include "eos/core/Image_opencv_interop.hpp"
#include "eos/core/Landmark.hpp"
#include "eos/core/LandmarkMapper.hpp"
#include "eos/core/read_pts_landmarks.hpp"
#include "eos/fitting/fitting.hpp"
#include "eos/fitting/RenderingParameters.hpp"
#include "eos/fitting/linear_shape_fitting.hpp"
#include "eos/fitting/orthographic_camera_estimation_linear.hpp"
#include "eos/morphablemodel/MorphableModel.hpp"
#include "eos/render/texture_extraction.hpp"
#include "eos/render/utils.hpp"
#include "eos/render/draw_utils.hpp"
//#include "eos/core/Mesh.hpp"
 //#include "eos/fitting/contour_correspondence.hpp"
 //#include "eos/fitting/closest_edge_fitting.hpp"
 //#include "eos/render/render.hpp"
 //#include "eos/core/Image_opencv_interop.hpp"

using namespace eos;
using eos::core::Landmark;
using eos::core::LandmarkCollection;
using Eigen::Vector2f;
using Eigen::Vector3f;
using Eigen::Vector4f;
using Eigen::VectorXf;
using Eigen::MatrixXf;

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

#include "utils.h"

using namespace superviseddescent;
namespace po = boost::program_options;
namespace fs = boost::filesystem;
namespace pt = boost::property_tree;
using std::vector;
using std::string;
using std::cout;
using std::endl;
//using boost::thread;

string selection;
cv::CascadeClassifier face_cascade;
dlib::frontal_face_detector detector_dlib;
string folder;
string subfolder;
string subfolder_nofaces;
string folder_correct;
string subfolder_correct[11];
string subfolder_discarded; //images discarded will go to subfolder_discarded so that they don't apear again when re-executing the program
rcr::detection_model rcr_model;
vector<vector<string>> csvdata;
bool first = true;
int cycles = 0;
unsigned long int total_cycles = 0;
string emotionlist[11] = { "Neutral", "Happy", "Sad", "Surprise", "Fear", "Disgust", "Anger", "Contempt", "None", "Uncertain", "non-face" };

/**
 * This app demonstrates the robust cascaded regression landmark detection from
 * "Random Cascaded-Regression Copse for Robust Facial Landmark Detection", 
 * Z. Feng, P. Huber, J. Kittler, W. Christmas, X.J. Wu,
 * IEEE Signal Processing Letters, Vol:22(1), 2015.
 *
 * It loads a model trained with rcr-train, detects a face using OpenCV's face
 * detector, and then runs the landmark detection.
 */

//void DrawPredictedImage(cv::Mat image, BoundingBox& bbox, cv::Mat_<double>& shape) {
//	unsigned long int t0, t1;
//	t0 = clock();
//	cv::rectangle(image, cv::Point(bbox.start_x, bbox.start_y), cv::Point(bbox.start_x + bbox.width, bbox.start_y + bbox.height), (255), 2);
//	for (int i = 0; i < shape.rows; i++) {
//		cv::circle(image, cv::Point2f(shape(i, 0), shape(i, 1)), 2, (255));
//	}
//	cv::imshow("show image", image);
//	t1 = clock();
//	std::cout << "time drawing faces: " << t1 - t0 << "ms" << std::endl;
//	cv::waitKey(0);
//}

void get_files_from_folder(vector<fs::path>& images_filenames, string directory, string extension) {
	fs::directory_iterator end_itr;
	int count_folders = 0;
	int count_files = 0;
	for (fs::directory_iterator i(directory); i != end_itr; ++i)
	{
		if (fs::is_directory(i->status())) {
			get_files_from_folder(images_filenames, i->path().string(), extension);
			count_folders++;
			//cout << "loading from folder" << endl;
		}
		else if (fs::is_regular_file(i->status()) && i->path().extension() == extension) {
			images_filenames.emplace_back(i->path());
			count_files++;
			//cout << "loading file" << endl;
		}
		if (LIMIT_LOAD_FOR_DEBUGGING == true) {
			if (count_folders >= MAX_NUM_FOLDERS_LOAD)
				break;
			if (count_files >= MAX_NUM_FILES_LOAD)
				break;
		}
	}
}

//from http://answers.opencv.org/question/55210/reading-csv-file-in-opencv/ modified for my needs
void get_data_from_csv(const string filename, vector<vector<string>>& matrix) {
	std::ifstream inputfile(filename);
	string current_line;
	matrix.clear();

	while(getline(inputfile, current_line)) {
		vector<string> vect;
		std::stringstream temp(current_line);
		string single_value;
		while (getline(temp, single_value, ',')) {
			vect.push_back(single_value);
		}
		matrix.push_back(vect);
	}
}

int find_divide_and_conquer(const vector<vector<string>>& matrix, const string& name, int& first, int& last, string& landmarks_str) {
	//obtain middle index
	int middle = first + (last-first) / 2;
	//cout << "searching between " << first << " and " << last << " (middle: " << middle << ")" << endl;

	//finish if there are no elements between first and last (this happens when middle==fisrt)
	if (middle == first) {
		//cout << "not found" << endl;
		return -1;
	}

	//get value to be compared
	string namestored = matrix[middle][0];
	//cout << "\t\t\tcomparing:" << endl;
	//cout << "\t\t\t" << namestored << endl;
	//cout << "\t\t\t" << name << endl;

	//divide and conquer
	int comp = namestored.compare(name);
	if (comp == 0) {
		//cout << "found at " << middle << endl;
		landmarks_str = matrix[middle][2].c_str();
		//cout << landmarks_str << endl;
		return atoi(matrix[middle][1].c_str());
	}
	else if (comp>0)
		return find_divide_and_conquer(matrix, name, first, middle, landmarks_str);
	else {
		return find_divide_and_conquer(matrix, name, middle, last, landmarks_str);
	}
}

int find_emotion(const vector<vector<string>>& matrix, const string& name, string& landmarks_str) {
	int first = 1; //line 0 are headers
	int last = matrix.size()-1;

	//check first and last, which are never compared in the divide and conquer algorithm
	string namestored = matrix[first][0];
	int comp = namestored.compare(name);
	if (comp == 0) {
		landmarks_str = matrix[first][2].c_str();
		//cout << landmarks_str << endl;
		return atoi(matrix[first][1].c_str());
	}

	namestored = matrix[last][0];
	comp = namestored.compare(name);
	if (comp == 0) {
		landmarks_str = matrix[last][2].c_str();
		//cout << landmarks_str << endl;
		return atoi(matrix[last][1].c_str());
	}

	return find_divide_and_conquer(matrix, name, first, last, landmarks_str);
}

//template<class LandmarkType> using LandmarkCollection = std::vector<Landmark<LandmarkType>>;
//cv::Mat vec2fToMat(LandmarkCollection<cv::Vec2f> landmarks)
//{
//	cv::Mat mat;
//	auto length = std::max(landmarks.cols, landmarks.rows); //landmarks is one columns with 68*2 rows (or vice versa)
//	for (int i = 0; i < length; ++i) {
//		landmarks.at<float>(i);
//	}
//	
//}

//from https://gist.github.com/zhou-chao
void writeCSV(string filename, cv::Mat mat)
{
	std::ofstream myfile;
	myfile.open(filename.c_str());
	myfile << cv::format(mat, cv::Formatter::FMT_CSV) << std::endl;
	myfile.close();
}

void split(const string& s, char delimiter, vector<string>& tokens)
{
	std::string token;
	std::istringstream tokenStream(s);
	while (std::getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
}

/**
* Calculate the norm (L2 error, in pixel) of two landmarks.
*
* @param[in] prediction First landmark.
* @param[in] groundtruth Second landmark.
* @return The L2 norm of the two landmark's coordinates.
*/
double norm(const rcr::Landmark<cv::Vec2f>& prediction, const rcr::Landmark<cv::Vec2f>& groundtruth)
{
	return cv::norm(prediction.coordinates, groundtruth.coordinates, cv::NORM_L2);
};

/**
* Calculate the element-wise L2 norm of two sets of landmarks.
*
* Requires both LandmarkCollections to have the same size.
*
* @param[in] prediction First set of landmarks.
* @param[in] groundtruth Second set of landmarks.
* @return A row-vector with each entry being the L2 norm of the two respective landmarks.
*/
cv::Mat elementwise_norm(const rcr::LandmarkCollection<cv::Vec2f>& prediction, const rcr::LandmarkCollection<cv::Vec2f>& groundtruth)
{
	assert(prediction.size() == groundtruth.size());
	cv::Mat result(1, prediction.size(), CV_32FC1); // a row with each entry a norm
	for (std::size_t i = 0; i < prediction.size(); ++i) {
		result.at<float>(i) = norm(prediction[i], groundtruth[i]);
	}
	return result;
};

/**
* Calculate the element-wise L2 norm of two sets of landmark
* collections, normalised by the inter eye distance of the
* predictions. Each row in the given matrices should correspond
* to one LandmarkCollection.
*
* \c [right|left]_eye_identifiers are used to calculate the inter eye distance.
*
* Requires both LandmarkCollections to have the same size.
*
* Note:
*  double mean_error = cv::mean(normalised_errors)[0]; // = the mean over all, identical to simple case
*  cv::reduce(normalised_errors, normalised_errors, 0, CV_REDUCE_AVG); // reduce to one row
*
* @param[in] prediction First set of landmarks.
* @param[in] groundtruth Second set of landmarks.
* @param[in] model_landmarks A mapping from indices to landmark identifiers.
* @param[in] right_eye_identifiers A list of landmark identifiers that specifies which landmarks make up the right eye.
* @param[in] left_eye_identifiers A list of landmark identifiers that specifies which landmarks make up the left eye.
* @return A matrix where each row corresponds to a separate set of landmarks (i.e. a different image), and each column is a different landmark's L2 norm.
*/
cv::Mat calculate_normalised_landmark_errors(cv::Mat predictions, cv::Mat groundtruth, vector<string> model_landmarks, vector<string> right_eye_identifiers, vector<string> left_eye_identifiers)
{
	assert(predictions.rows == groundtruth.rows && predictions.cols == groundtruth.cols);
	cv::Mat normalised_errors;
	for (int r = 0; r < predictions.rows; ++r) {
		auto pred = rcr::to_landmark_collection(predictions.row(r), model_landmarks);
		auto gt = rcr::to_landmark_collection(groundtruth.row(r), model_landmarks);
		// calculates the element-wise norm, normalised with the IED (interEyedDistance):
		cv::Mat landmark_norms = elementwise_norm(pred, gt).mul(1.0f / rcr::get_ied(pred, right_eye_identifiers, left_eye_identifiers));
		normalised_errors.push_back(landmark_norms);
	}
	return normalised_errors;
};

/**
* Reads a list of which landmarks to train.
*
* @param[in] configfile A training config file to read.
* @return A list that contains all the landmark identifiers from the config that are to be used for training.
*/
vector<string> read_landmarks_list_to_train(fs::path configfile)
{
	pt::ptree config_tree;
	pt::read_info(configfile.string(), config_tree); // Can throw a pt::ptree_error, maybe try/catch

	vector<string> model_landmarks;
	// Get stuff from the modelLandmarks subtree:
	pt::ptree pt_model_landmarks = config_tree.get_child("modelLandmarks");
	string model_landmarks_usage = pt_model_landmarks.get<string>("landmarks");
	if (model_landmarks_usage.empty()) {
		// value is empty, meaning it's a node and the user should specify a list of 'landmarks'
		pt::ptree pt_model_landmarks_list = pt_model_landmarks.get_child("landmarks");
		for (const auto& kv : pt_model_landmarks_list) {
			model_landmarks.push_back(kv.first);
		}
		cout << "Loaded a list of " << model_landmarks.size() << " landmarks to train the model." << endl;
	}
	else if (model_landmarks_usage == "all") {
		throw std::logic_error("Using 'all' modelLandmarks is not implemented yet - specify a list for now.");
	}
	else {
		throw std::logic_error("Error reading the models 'landmarks' key, should either provide a node with a list of landmarks or specify 'all'.");
	}
	return model_landmarks;
};

/**
* Reads a config file ('eval.txt') that specifies which landmarks make
* up the eyes and are to be used to calculate the IED (interEyedDistance).
*
* @param[in] evaluationfile A training config file to read.
* @return A pair with the right and left eye identifiers.
* @throws A ptree or logic error?
*/
std::pair<vector<string>, vector<string>> read_how_to_calculate_the_IED(fs::path evaluationfile)
{
	vector<string> right_eye_identifiers, left_eye_identifiers;

	pt::ptree eval_config_tree;
	string right_eye;
	string left_eye;
	pt::read_info(evaluationfile.string(), eval_config_tree); // could throw a boost::property_tree::ptree_error, maybe try/catch

	pt::ptree pt_parameters = eval_config_tree.get_child("interEyeDistance");
	right_eye = pt_parameters.get<string>("rightEye");
	left_eye = pt_parameters.get<string>("leftEye");

	// Process the interEyeDistance landmarks - one or two identifiers might be given
	boost::split(right_eye_identifiers, right_eye, boost::is_any_of(" "));
	boost::split(left_eye_identifiers, left_eye, boost::is_any_of(" "));
	return std::make_pair(right_eye_identifiers, left_eye_identifiers);
}

/**
* @brief Draws 3D axes onto the top-right corner of the image. The
* axes are oriented corresponding to the given angles.
*
* @param[in] r_x Pitch angle, in radians.
* @param[in] r_y Yaw angle, in radians.
* @param[in] r_z Roll angle, in radians.
* @param[in] image The image to draw onto.
*/
void draw_axes_topright(float r_x, float r_y, float r_z, cv::Mat image)
{
	const glm::vec3 origin(0.0f, 0.0f, 0.0f);
	const glm::vec3 x_axis(1.0f, 0.0f, 0.0f);
	const glm::vec3 y_axis(0.0f, 1.0f, 0.0f);
	const glm::vec3 z_axis(0.0f, 0.0f, 1.0f);

	const auto rot_mtx_x = glm::rotate(glm::mat4(1.0f), r_x, glm::vec3{ 1.0f, 0.0f, 0.0f });
	const auto rot_mtx_y = glm::rotate(glm::mat4(1.0f), r_y, glm::vec3{ 0.0f, 1.0f, 0.0f });
	const auto rot_mtx_z = glm::rotate(glm::mat4(1.0f), r_z, glm::vec3{ 0.0f, 0.0f, 1.0f });
	const auto modelview = rot_mtx_z * rot_mtx_x * rot_mtx_y;

	const auto viewport = fitting::get_opencv_viewport(image.cols, image.rows);
	const float aspect = static_cast<float>(image.cols) / image.rows;
	const auto ortho_projection = glm::ortho(-3.0f * aspect, 3.0f * aspect, -3.0f, 3.0f);
	const auto translate_topright = glm::translate(glm::mat4(1.0f), glm::vec3(0.7f, 0.65f, 0.0f));
	const auto o_2d = glm::project(origin, modelview, translate_topright * ortho_projection, viewport);
	const auto x_2d = glm::project(x_axis, modelview, translate_topright * ortho_projection, viewport);
	const auto y_2d = glm::project(y_axis, modelview, translate_topright * ortho_projection, viewport);
	const auto z_2d = glm::project(z_axis, modelview, translate_topright * ortho_projection, viewport);
	cv::line(image, cv::Point2f{ o_2d.x, o_2d.y }, cv::Point2f{ x_2d.x, x_2d.y }, { 0, 0, 255 });
	cv::line(image, cv::Point2f{ o_2d.x, o_2d.y }, cv::Point2f{ y_2d.x, y_2d.y }, { 0, 255, 0 });
	cv::line(image, cv::Point2f{ o_2d.x, o_2d.y }, cv::Point2f{ z_2d.x, z_2d.y }, { 255, 0, 0 });
};

inline cv::Mat to_col(vector<Vector3f> landmarks)
{
	// landmarks.size() must be <= max_int
	auto num_landmarks = static_cast<int>(landmarks.size());
	cv::Mat col(num_landmarks * 3, 1, CV_32FC1);
	for (int i = 0; i < num_landmarks; ++i) {
		col.at<float>(i) = landmarks[i].x();
		col.at<float>(i + num_landmarks) = landmarks[i].y();
		col.at<float>(i + num_landmarks*2) = landmarks[i].z();
	}
	return col;
}

int main(int argc, char *argv[])
{
	/*fs::path facedetector, paramsfile, testing_config_file, outputfile;
	try {
		po::options_description desc("Allowed options");
		desc.add_options()
			("help,h",
				"display the help message")
			("facedetector,f", po::value<fs::path>(&facedetector)->required()->default_value("data/haarcascade_frontalface_alt2.xml"),
				"full path to OpenCV's face detector (haarcascade_frontalface_alt2.xml)")
			("params,p", po::value<fs::path>(&paramsfile)->required()->default_value("config_testing.txt"),
				"parameters for the program")
			("image,i", po::value<fs::path>(&testing_config_file)->required()->default_value("config_testingset.txt"),
				"input image file")
			("output,o", po::value<fs::path>(&outputfile)->required()->default_value("results/296961468_1_result.jpg"),
				"filename for the result image")
			;
		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
		if (vm.count("help")) {
			cout << "Usage: rcr-detect [options]" << endl;
			cout << desc;
			system("pause");
			return EXIT_SUCCESS;
		}
		po::notify(vm);
	}
	catch (const po::error& e) {
		cout << "Error while parsing command-line arguments: " << e.what() << endl;
		cout << "Use --help to display a list of options." << endl;
		system("pause");
		return EXIT_SUCCESS;
	}*/

	string facedetector, paramsfile, testing_config_file, database_config_file, outputfile;
	facedetector = "data/haarcascade_frontalface_alt2.xml";
	paramsfile = "config_databaseLandmarks.txt";
	testing_config_file = "config_testingset.txt";
	database_config_file = "config_classify.txt";
	outputfile = "results/296961468_1_result.jpg";

	// Read parameters
	string* params;

	string modelfolder;
	string modelfile;
	params = new string[2];
	get_params(paramsfile, params, 2);
	modelfolder = params[0];
	modelfile = params[1];
	cout << "model folder: " << modelfolder << endl;
	cout << "model file: " << modelfile << endl;

	params = new string[1];
	get_params(modelfolder+"model_params.txt", params, 1);
	selection = params[0];
	cout << "selection: " << selection << endl;

	// Load the learned model:
	try {
		rcr_model = rcr::load_detection_model(modelfolder + modelfile);
	}
	catch (const cereal::Exception& e) {
		cout << "Error reading the RCR model " << modelfolder + modelfile << ": " << e.what() << endl;
		system("pause");
		return EXIT_FAILURE;
	}

	// Load the face detector from OpenCV:
	if (!face_cascade.load(facedetector))
	{
		cout << "Error loading the face detector " << facedetector << "." << endl;
		system("pause");
		return EXIT_FAILURE;
	}
	detector_dlib = dlib::get_frontal_face_detector();
	
	//core::LandmarkMapper landmark_mapper;
	//string mappingsfile = "data/ibug_to_sfm.txt";
	//try
	//{
	//	landmark_mapper = core::LandmarkMapper(mappingsfile);
	//}
	//catch (const std::exception& e)
	//{
	//	cout << "Error loading the landmark mappings: " << e.what() << endl;
	//	system("pause");
	//	return EXIT_FAILURE;
	//}

	//morphablemodel::MorphableModel morphable_model;
	//string morphablemodelfile = "data/sfm_shape_3448.bin";
	//try
	//{
	//	morphable_model = morphablemodel::load_model(morphablemodelfile);
	//}
	//catch (const std::runtime_error& e)
	//{
	//	cout << "Error loading the Morphable Model: " << e.what() << endl;
	//	system("pause");
	//	return EXIT_FAILURE;
	//}

	//vector<string> model_landmarks; // list read from the files, might be 68 or less
	//try {
	//	model_landmarks = read_landmarks_list_to_train("data/rcr_training_68.cfg");
	//}
	//catch (const pt::ptree_error& e) {
	//	cout << "Error reading the training config: " << e.what() << endl;
	//	system("pause");
	//	return EXIT_FAILURE;
	//}
	//catch (const std::logic_error& e) {
	//	cout << "Parsing config: " << e.what() << endl;
	//	system("pause");
	//	return EXIT_FAILURE;
	//}

	//vector<string> right_eye_identifiers, left_eye_identifiers; // for ied calc. One or several.
	//try {
	//	std::tie(right_eye_identifiers, left_eye_identifiers) = read_how_to_calculate_the_IED("data/rcr_eval.cfg");
	//}
	//catch (const pt::ptree_error& e) {
	//	cout << "Error reading the evaluation config: " << e.what() << endl;
	//	system("pause");
	//	return EXIT_FAILURE;
	//}
	//catch (const std::logic_error& e) {
	//	cout << "Parsing config: " << e.what() << endl;
	//	system("pause");
	//	return EXIT_FAILURE;
	//}

	/*cout << "loading blendshapes2" << endl;
	const morphablemodel::Blendshapes blendshapes0 = morphablemodel::load_blendshapes("data/expression_blendshapes_3448.bin");
	cout << "loading edge_topology2" << endl;
	const morphablemodel::EdgeTopology edge_topology0 = morphablemodel::load_edge_topology("data/sfm_3448_edge_topology.json");
	cout << "loading model_contour2" << endl;
	const fitting::ModelContour model_contour0 = fitting::ModelContour::load("data/sfm_model_contours.json");
	cout << "loading ibug_contour2" << endl;
	const fitting::ContourLandmarks ibug_contour0 = fitting::ContourLandmarks::load("data/ibug_to_sfm.txt");*/

	//open file with databases paths
	std::ifstream fin;
	fin.open(database_config_file, std::ifstream::in);
	fin >> folder_correct;
	fin >> subfolder_discarded;
	int num_datasets;
	fin >> num_datasets;

	//create folder to save correct images if it doesn't exist


	cout << "\ncorrect images will be saved to: " << folder_correct << endl;
	if (!fs::exists(folder_correct))
		fs::create_directory(folder_correct);
	cout << "\ncorrect images of each emotion will be saved to: " << endl;
	for (int i = 0; i < 11; i++) {
		subfolder_correct[i] = folder_correct + "emotion_" + std::to_string(i) + "/";
		cout << subfolder_correct[i] << endl;
		if (!fs::exists(subfolder_correct[i]))
			fs::create_directory(subfolder_correct[i]);
	}
	cout << "\ndiscarded images will be saved to: " << subfolder_discarded << endl;
	if (!fs::exists(subfolder_discarded))
		fs::create_directory(subfolder_discarded);
	

	get_data_from_csv("C:/Users/pmenendezblanco/VisualStudio_projects/Emotion/Databases/Emotion_databases/AffectNet/all_ordered.csv", csvdata);

	string datapath, extension;
	for (int i = 0; i < num_datasets; i++) {
		fin >> datapath;
		fin >> extension;
		cout << "path " << datapath << " with extension " << extension << endl;
				
		// Get all the filenames in the given directory:
		//load_data(images, datapath, extension);
		vector<fs::path> images_filenames;
		get_files_from_folder(images_filenames, datapath, extension);


		// Load each image, obtain landmarks and store them
		unsigned long int tt0, tt1;
		tt0 = clock();
		tt1 = tt0;
		for (auto file : images_filenames)
		{
			bool classified = false;
			for (int i = 0; i < 11; i++) {
				//subfolder_correct[i] = folder_correct + "emotion_" + std::to_string(i) + "/";
				if (fs::exists(subfolder_correct[i] + file.filename().string())) {
					cout << "file already classified" << endl;
					classified = true;
				}	
			}
			if (fs::exists(subfolder_discarded + file.filename().string())) {
				cout << "file already classified" << endl;
				classified = true;
			}
			if (classified)
				continue;

			if (PRINT_MSGS) cout << endl << endl;

			cv::Mat image;
			cv::Mat image_original;
			cv::Mat image_results;
			string landmarks_str;
			int e = find_emotion(csvdata, file.filename().string(), landmarks_str);
			cout << "\nemotion found: " << e << " -> " << emotionlist[e] << endl;
			if (e == -1) //if emotion label not found
				continue;
			if (EMOTION_TO_FIND != -1 && e != EMOTION_TO_FIND) //if emotion is not the one we are looking for
				continue;

			//get true landmarks (to later calculate error)
			//vector<string> landmarks_tr;
			//split(landmarks_str, ';', landmarks_tr); //true landmarks
			////cout << landmarks_tr.size() << " true landmarks found" << endl;
			//if (landmarks_tr.size() < 136) {
			//	if (PRINT_MSGS)	cout << "not enough landmarks" << endl;
			//	continue;
			//}

			//cv::Mat landmarks_tr_col(landmarks_tr.size(), 1, CV_32FC1);
			//for (int i = 0; i < landmarks_tr.size(); i++) {
			//	if(i%2==0)
			//		landmarks_tr_col.at<float>(i/2) = atof(landmarks_tr[i].c_str());
			//	else
			//		landmarks_tr_col.at<float>(i/2+landmarks_tr.size()/2) = atof(landmarks_tr[i].c_str());
			//}
			////cv::Mat landmarks_tr_row(1, landmarks_tr.size(), CV_32FC1);
			//cv::Mat landmarks_tr_row;
			//cv::transpose(landmarks_tr_col, landmarks_tr_row);
			///*cout << "\nlandmarks_tr , landmarks_tr_col" << endl;
			//for (int i = 0; i < landmarks_tr_col.size().height; i++)
			//	cout << i << ": " << landmarks_tr[i] << "," << landmarks_tr_col.at<float>(i) << endl;*/

			image = cv::imread(file.string());
			image_original = image.clone();
			//myresize(image);

			if (PRINT_MSGS) {
				//cout << "\t" << file.string() << endl; //file.string() gives full path
				cout << "\t" << file.filename() << endl;
				cout << "\timage: " << image.cols << " x " << image.rows << endl;
			}
			if (image.cols == 0 || image.rows == 0)
				continue;

			cv::Mat_<uchar> image_gray;
			cvtColor(image, image_gray, CV_BGR2GRAY);

			unsigned long int t0, t1;
			//use OpenCV detector or dlib detector
			vector<cv::Rect> detected_faces;
			if (selection == "opencv") {
				unsigned long int t0, t1;
				//t0 = clock();
				face_cascade.detectMultiScale(image_gray, detected_faces, 1.2, 2, 0, cv::Size(50, 50));
				//t1 = clock();
				//cout << "\ttime detecting faces with opencv: " << t1 - t0 << " ms" << endl;
			}
			else {
				//dlib::array2d<dlib::bgr_pixel> image_dlib_color;
				//dlib::assign_image(image_dlib_color, dlib::cv_image<dlib::bgr_pixel>(images[i]));

				dlib::array2d<unsigned char> image_gray_dlib;
				dlib::assign_image(image_gray_dlib, dlib::cv_image<unsigned char>(image_gray));

				//load_image(image_dlib, image_path.c_str());
				vector<dlib::rectangle> dets;
				unsigned long int t0, t1;
				//t0 = clock();
				dets = detector_dlib(image_gray_dlib);
				//t1 = clock();
				//cout << "\n\ttime detecting faces with dlib: " << t1 - t0 << " ms" << endl;
				for (int i = 0; i < dets.size(); i++) {
					detected_faces.push_back(dlibRectangleToOpenCV(dets[i]));
				}
			}

			if (detected_faces.empty()) {
				if (PRINT_MSGS) cout << "\tNO FACES DETECTED" << endl;
				//save images for which faces were not detected, in a separate folder to analyze them
				//if (SAVE_PROCESSED_IMAGE)
					//cv::imwrite(subfolder_nofaces + file.filename().string(), image);
				continue;
			}
			
			for (int j = 0; j < detected_faces.size(); j++) {
				if (PRINT_MSGS) cout << "\trectangle: (" << detected_faces[j].x << "," << detected_faces[j].y << ") + " << detected_faces[j].width << "x" << detected_faces[j].height << endl;
				// Detect the landmarks:
				if (PRINT_MSGS) t0 = clock();
				auto landmarks_collection = rcr_model.detect(image, detected_faces[j]);
				if (PRINT_MSGS) { t1 = clock(); cout << "\t\ttime detecting landmarks: " << t1 - t0 << " ms" << endl; }
				//cv::Mat landmarks_row = to_row(landmarks_collection);
				cv::Mat landmarks_col = to_col(landmarks_collection);

				////GETTING THE ERROR
				////cout << landmarks_col.size() << "==" << landmarks_tr_col.size() << "?" << endl;
				///*for (int i = 0; i < landmarks_col.size().height; i++)
				//	cout << landmarks_col.at<float>(i) << " , " << landmarks_tr_col.at<float>(i) << endl;*/
				//float NLSRerror = cv::norm(landmarks_col, landmarks_tr_col, cv::NORM_L2) / cv::norm(landmarks_tr_col, cv::NORM_L2);
				//if (PRINT_MSGS) cout << "\t\t\t\t\t\t\tNLSR error: " << NLSRerror << endl;
				//cv::Mat normalised_error = calculate_normalised_landmark_errors(landmarks_row, landmarks_tr_row, model_landmarks, right_eye_identifiers, left_eye_identifiers);
				//float NORMerror = cv::mean(normalised_error)[0];
				//cout << "\t\t\t\t\t\t\tNormalised error: " << NORMerror << endl;



				//ADD DATA TO MAT
				if (PRINT_MSGS) cout << "\t\temotion: " << e << " -> " << emotionlist[e] << endl;
				
				//Draw on the image
				cv::rectangle(image, detected_faces[j], cv::Scalar(0, 0, 255), 2); //BGR
				//render::draw_wireframe(image, mesh2, rendering_params2.get_modelview(), rendering_params2.get_projection(), fitting::get_opencv_viewport(image.cols, image.rows));
				rcr::draw_landmarks(image, landmarks_collection, cv::Scalar(255, 0, 0)); //true landmarks in purple
				//auto landmarks_tr_collection = rcr::to_landmark_collection(landmarks_tr_row, model_landmarks);
				//rcr::draw_landmarks(image, landmarks_tr_collection, cv::Scalar(255, 0, 255)); //true landmarks in purple
				//draw_axes_topright(pitch_radians, yaw_radians, roll_radians, image);

				
			}

			//show image
			if (SHOW_PROCESSED_IMAGE) {
				cv::imshow("show image", image);
				char c = cv::waitKey(0);
				int i = c - '0';
				cout << "letter input: " << c << endl;
				cout << "\n\tsaving processed image" << endl;
				string name = file.filename().string();
				if (EMOTION_TO_FIND!=-1) { //if we are looking for a certain emotion
					if (c == 'y')
						cv::imwrite(subfolder_correct[EMOTION_TO_FIND] + name, image_original);
					else
						cv::imwrite(subfolder_discarded + name, image_original);
				}
				else {
					if (i >= 0 && i <= 9) {
						cout << "int input: " << i << endl;
						cv::imwrite(subfolder_correct[i] + name, image_original);
					}
					else
						cv::imwrite(subfolder_discarded + name, image_original);
				}

			}

			
		}
	}


	system("pause");
	return EXIT_SUCCESS;
}
