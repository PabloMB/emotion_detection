#pragma once

#include <vector>

template <typename T>
float mean(std::vector<T>& arr);

template <typename T>
float standard_deviation(std::vector<T>& arr, float mean);
