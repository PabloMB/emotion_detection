
#include "basic_statistics.h"

#include <cmath>
#include <vector>

template <typename T>
float mean(std::vector<T>& arr) {
	float sum = 0;
	for (int i = 0; i < arr.size(); i++) {
		sum += arr[i];
	}
	return sum / arr.size();
}

template <typename T>
float standard_deviation(std::vector<T>& arr, float mean) {
	float sum = 0;
	for (int i = 0; i < arr.size(); i++) {
		sum += pow(arr[i] - mean, 2);
	}
	return sqrt(sum / arr.size());
}