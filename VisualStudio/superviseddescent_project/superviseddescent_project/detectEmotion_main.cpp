/*
 * superviseddescent: A C++11 implementation of the supervised descent
 *                    optimisation method
 * File: apps/rcr/rcr-detect.cpp
 *
 * Copyright 2015 Patrik Huber
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define _CRT_SECURE_NO_WARNINGS  //to avoid error C4996 with localtime (unsafe function)


#include "detectEmotion_fromAndroid.hpp"
#include <opencv2/videoio.hpp>
#include <opencv2/highgui/highgui.hpp> 
#include <opencv2/imgcodecs.hpp>
using namespace cv;
using namespace std;

bool show_wireframe = false;

void overlayImage(const cv::Mat &background, const cv::Mat &foreground,
	cv::Mat &output, cv::Point2i location)
{
	background.copyTo(output);


	// start at the row indicated by location, or at row 0 if location.y is negative.
	for (int y = std::max(location.y, 0); y < background.rows; ++y)
	{
		int fY = y - location.y; // because of the translation

								 // we are done of we have processed all rows of the foreground image.
		if (fY >= foreground.rows)
			break;

		// start at the column indicated by location, 

		// or at column 0 if location.x is negative.
		for (int x = std::max(location.x, 0); x < background.cols; ++x)
		{
			int fX = x - location.x; // because of the translation.

									 // we are done with this row if the column is outside of the foreground image.
			if (fX >= foreground.cols)
				break;

			// determine the opacity of the foregrond pixel, using its fourth (alpha) channel.
			double opacity =
				((double)foreground.data[fY * foreground.step + fX * foreground.channels() + 3])

				/ 255.;


			// and now combine the background and foreground pixel, using the opacity, 

			// but only if opacity > 0.
			for (int c = 0; opacity > 0 && c < output.channels(); ++c)
			{
				unsigned char foregroundPx =
					foreground.data[fY * foreground.step + fX * foreground.channels() + c];
				unsigned char backgroundPx =
					background.data[y * background.step + x * background.channels() + c];
				output.data[y*output.step + output.channels()*x + c] =
					backgroundPx * (1. - opacity) + foregroundPx * opacity;
			}
		}
	}
}

Mat place_png(Mat srcImage, Mat frame) {
	Mat mask;
	vector<Mat> rgbLayers;
	split(srcImage, rgbLayers);
	
	if (srcImage.channels() == 4)
	{
		cout << "splitting image" << endl;
		split(srcImage, rgbLayers);									 // seperate channels
		cout << "splitting image1" << endl;
		Mat cs[3] = { rgbLayers[0],rgbLayers[1],rgbLayers[2] };
		cout << "splitting image2" << endl;
		merge(cs, 3, srcImage);										 // glue together again
		cout << "splitting image3" << endl;
		mask = rgbLayers[3];										 // png's alpha channel used as mask
		cout << "splitting image4" << endl;
	}

	srcImage.copyTo(frame(cv::Rect(0, 0, srcImage.cols, srcImage.rows)), mask);

	return frame;
}

//int main1() {
//	
//	string testing_config_file = "config_testingset.txt";
//	vector<cv::Mat> images;
//	load_data(images, testing_config_file);
//	if (images.empty()) {
//		cout << "ERROR: no images found" << endl;
//		system("pause");
//		return -1;
//	}
//
//	EDetector edetector;
//	string output;
//	edetector.load("fromAndroid/data", output);
//
//	float max;
//	float* emotions;
//	int emotion = -1;
//	unsigned long int t0, t1;
//	for (int i = 0; i < images.size(); i++) {
//
//		float* emotions = edetector.detectEmotion(images[i]);
//		max = 0;
//		emotion = -1;
//		for (int i = 0; i<8; i++) {
//			if (emotions[i]>max) {
//				max = emotions[i];
//				emotion = i;
//			}
//		}
//		cout << "emotion: " << emotion << endl;
//
//	}
//	
//	system("pause");
//	return 0;
//}

int main2() {
	VideoCapture cap;
	cap.open(0);
	if (!cap.isOpened()) {
		cerr << "ERROR! Unable to open camera\n";
		system("pause");
		return -1;
	}
	string window3 = "Emotion";
	Mat emoji_image;
	Mat emoji_image_base;
	Mat emoji_images[8];
	emoji_images[0] = cv::imread("fromAndroid/emojis/emoji_neutral1.png", IMREAD_UNCHANGED);
	emoji_images[1] = cv::imread("fromAndroid/emojis/emoji_happy1.png",IMREAD_UNCHANGED);
	emoji_images[2] = cv::imread("fromAndroid/emojis/emoji_sad1.png", IMREAD_UNCHANGED);
	emoji_images[3] = cv::imread("fromAndroid/emojis/emoji_surprise1.png", IMREAD_UNCHANGED);
	emoji_images[4] = cv::imread("fromAndroid/emojis/emoji_fear1.png", IMREAD_UNCHANGED);
	emoji_images[5] = cv::imread("fromAndroid/emojis/emoji_angry1.png", IMREAD_UNCHANGED);
	emoji_images[6] = cv::imread("fromAndroid/emojis/emoji_disgust1.png", IMREAD_UNCHANGED);
	emoji_images[7] = cv::imread("fromAndroid/emojis/emoji_contempt1.png", IMREAD_UNCHANGED);

	resize(emoji_images[0], emoji_images[0], cv::Size(), 0.25, 0.25);
	resize(emoji_images[1], emoji_images[1], cv::Size(), 0.25, 0.25);
	resize(emoji_images[2], emoji_images[2], cv::Size(), 0.25, 0.25);
	resize(emoji_images[3], emoji_images[3], cv::Size(), 0.25, 0.25);
	resize(emoji_images[4], emoji_images[4], cv::Size(), 0.25, 0.25);
	resize(emoji_images[5], emoji_images[5], cv::Size(), 0.25, 0.25);
	resize(emoji_images[6], emoji_images[6], cv::Size(), 0.25, 0.25);
	resize(emoji_images[7], emoji_images[7], cv::Size(), 0.25, 0.25);

	Mat emoji_images2[8];
	Mat background = cv::Mat(emoji_images[0].size(), CV_8UC3, cv::Vec3b(255, 255, 255)); // white background
	overlayImage(background, emoji_images[0], emoji_images2[0], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[1].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[1], emoji_images2[1], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[2].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[2], emoji_images2[2], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[3].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[3], emoji_images2[3], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[4].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[4], emoji_images2[4], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[5].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[5], emoji_images2[5], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[6].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[6], emoji_images2[6], cv::Point2i(0, 0));
	background = cv::Mat(emoji_images[7].size(), CV_8UC3, cv::Vec3b(255, 255, 255));
	overlayImage(background, emoji_images[7], emoji_images2[7], cv::Point2i(0, 0));

	//for (int i = 0; i < 8; i++)
	//	emoji_images[i].release();

	emoji_image_base = cv::imread("fromAndroid/emojis/emoji_neutral1.png");
	resize(emoji_image_base, emoji_image_base, cv::Size(), 0.25, 0.25);
	emoji_image_base = cv::Scalar(255.0, 255.0, 255.0); //BGR


	EDetector edetector;
	string output;
	edetector.load("fromAndroid/data", output);

	Mat frame;
	float max;
	float* emotions;
	int emotion = -1;
	unsigned long int t0, t1, tcycle;
	for (;;)
	{
		t0 = clock();
		cap.read(frame);
		if (frame.empty()) {
			cerr << "ERROR! blank frame grabbed\n";
			break;
		}

		emotions = edetector.detectEmotion(frame);
		max = 0;
		emotion = -1;
		for (int i = 0; i<8; i++) {
			//cout << emotions[i] << " ";
			if (emotions[i]>max) {
				max = emotions[i];
				emotion = i;
			}
		}//cout << endl;
		cout << "emotion: " << emotion << endl;
		//emoji_image = emoji_images[emotion];
		cv::imshow(window3, emoji_images2[emotion]);

		////Mat img_emoji = place_png(emoji_image.clone(), emoji_image_base.clone());
		//Mat emoji_image2;
		//Mat background = cv::Mat(emoji_image.size(), CV_8UC3, cv::Vec3b(255, 255, 255)); // white background
		//overlayImage(background, emoji_image, emoji_image2, cv::Point2i(0, 0));
		////addWeighted(background, 0.5, emoji_image, 0.5, 0.0, emoji_image2);
		//cv::imshow(window3, emoji_image2);

		char c = waitKey(5);
		if (c == 'q')
			break;
		else if (c == '1')
			edetector.set_todo(1);
		else if (c == '2')
			edetector.set_todo(2);
		else if (c == '3')
			edetector.set_todo(3);
		else if (c == 'o')
			edetector.set_detector("opencv");
		else if (c == 'd')
			edetector.set_detector("dlib");
		else if (c == 's')
			cv::imwrite("output/face.jpg", frame);
		else if (c == 'w') {
			show_wireframe = !show_wireframe;
			edetector.show_wireframe(show_wireframe);
		}

		t1 = clock();
		tcycle = t1 - t0;
		cout << "\t\t\t\t\t\tTOTAL TIME: " << tcycle << " ms" << endl;
		cout << "\t\t\t\t\t\t\t\tFPS " << 1000.0/(tcycle) << endl;
	}

	return 0;
}

int main(int argc, char *argv[])
{
	return main2();
}